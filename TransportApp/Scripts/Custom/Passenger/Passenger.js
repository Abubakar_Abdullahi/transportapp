﻿
TransportApp.controller('PassengerCtrl', ['$scope', 'SharedServices', function ($scope, SharedServices) {

    $scope.Roles;
    $scope.Passengers;
    $scope.Token;

    $scope.init = function () {
        $scope.Token = Cookies.get("Token");
        $scope.GetPassengers();
        $scope.GetNotifications();
        $scope.GetRoles();
    };

    $scope.GetRoles = function () {
        SharedServices.Post('/User/GetRoles', { Token: $scope.Token }).then(function (myResponse) {
            if (myResponse.IsSuccessful) {
                $scope.Roles = myResponse.response.data;
            }   
            else
                console.log("An error occurred while getting Roles.");
        });
    };

    $scope.GetPassengers = function () {
        SharedServices.Post('/Passenger/GetPassengers', { Token: $scope.Token }).then(function (myResponse) {
            if (myResponse.IsSuccessful)
                $scope.Passengers = myResponse.response.data;
            else
                console.log("An error occurred while getting Passengers.");
        });
    };

    $scope.GetNotifications = function () {
        SharedServices.Post('/Notification/GetPassengerNotifications', { Token: $scope.Token }).then(function (myResponse) {
            if (myResponse.IsSuccessful) {
                $scope.Notifications = myResponse.response.data;
                for (i = 0; i < $scope.Notifications.length; i++)
                    $scope.Notifications[i].Datetime = new Date($scope.Notifications[i].Datetime * 1000);
            }
            else
                console.log("An error occurred while getting Notifications.");
        });
    };

    $scope.AddPassenger = function () {
        var data;
        for (var i = 0; i < $scope.Roles.length; i++) {
            if ($scope.Roles[i].RoleName === "Passenger") {
                data = $scope.Roles[i];
                break;
            }   
        }

        SharedServices.OpenModal("../Views/Passenger/PassengerForm.html", "FormCtrl", "lg", data, true, "/User/SaveUser", null, null).result.then(function () {
            $scope.init();
        });
    };

    $scope.PassengerDetails = function (passenger) {
        var passengerWindow = window.open('/Passenger/Details', '_self');
        var data = {
            Passenger: passenger,
            Trips: $scope.Trips
        };
        var dataStr = JSON.stringify(data);
        passengerWindow.passData = data;
        passengerWindow.sessionStorage.passData = dataStr;
    };

    $scope.DeletePassenger = function (passenger) {
        var data = {
            UserId: passenger.PassengerId,
            Token: $scope.Token
        };
        SharedServices.OpenModal("../Views/Shared/ConfirmDelete.html", "DeleteItemCtrl", "md", data, null, "/User/DeleteUser").result.then(function () {
            $scope.init();
        });
    };
}]);