﻿// Decompiled with JetBrains decompiler
// Type: TransportApp.Hubs.Startup
// Assembly: TransportApp, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9FC5505E-8A04-49A5-ADEF-D6AFF964F1CE
// Assembly location: C:\Users\Abubakar\Desktop\pfts\bin\TransportApp.dll

using Owin;

namespace TransportApp.Hubs
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}
