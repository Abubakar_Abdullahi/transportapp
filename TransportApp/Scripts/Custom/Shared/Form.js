﻿
TransportApp.controller('FormCtrl', ['$scope', '$uibModalInstance', 'passData', '$uibModal', 'SharedServices', function ($scope, $uibModalInstance, passData, $uibModal, SharedServices) {

    $scope.NewData = {};
    // HeadlineCheck is used to decide what to display as Modal Headline
    // NewData is not used as the 'decider' becuase once data is started to be filled in a new form, it treats the form as populated
    $scope.HeadlineCheck;
    $scope.flag = passData.flag;
    $scope.DataList1 = [];
    $scope.DataList2 = [];
    $scope.DataList3 = [];
    $scope.Token;
    $scope.AllOptions;

    $scope.init = function () {
        $scope.Token = Cookies.get("Token");
        if (passData.data)
            $scope.NewData = passData.data;
        // populates HeadlineCheck if the form is to be edited(i.e passData.data != null) else sets it to empty string
        $scope.HeadlineCheck = passData.data ? "Dummy String" : "";

        $scope.DataList1 = passData.dataList1;
        $scope.DataList2 = passData.dataList2;
        $scope.DataList3 = passData.dataList3;

        var imageInput = document.getElementById('img-upload-input');
        if (imageInput) // checks if the current form has an imageInput
            imageInput.addEventListener('change', $scope.handleFileSelect, false);
    };

    $scope.GetAllOptions = function () {
        SharedServices.Post($scope.NewData.AllDataUrl, { Token: $scope.Token}).then(function (myResponse) {
            if (myResponse.IsSuccessful) {
                $scope.AllOptions = myResponse.response.data;

                var data = {
                    SelectedOptions: $scope.NewData.Options ? Array.from($scope.NewData.Options).map(function (option) {
                        return {
                            Id: option.Id,
                            Label: option.Label
                        };
                    }) : [],
                    AllOptions: $scope.AllOptions.map(function (option) {
                        return {
                            Id: option.PassengerId || option.TripId,
                            Label: option.Name || option.TripName
                        };
                    })
                };
                $scope.SelectOptions(data);
            }
            else
                console.log("An error occurred while getting Options from " + $scope.NewData.AllDataUrl + ".");
        });
    };

    $scope.SelectOptions = function (data) {
        SharedServices.OpenModal('../Views/Shared/Options.html', 'OptionsCtrl', 'md', data).result.then(function (OptionsResult) {
            $scope.NewData.Options = [];
            for (i = 0; i < OptionsResult.SelectedOptions.length; i++)
                $scope.NewData.Options[i] = {
                    Id: OptionsResult.SelectedOptions[i].Id,
                    Label: OptionsResult.SelectedOptions[i].Label
                };
        });
    };

    $scope.handleFileSelect = function (evt) {
        var files = evt.target.files; //FileList object

        var reader = new FileReader();
        reader.onload = (function (theFile) {
            return function (e) {
                document.getElementById("profile-pic").src = e.target.result; // sets the Profile Picture for display before saving

                if (!$scope.NewData.ProfilePicture) // initialize ProfilePicture if it is undefined
                    $scope.NewData.ProfilePicture = {};

                // sets parameters in order to get ready for sending to server
                $scope.NewData.ProfilePicture.ImageName = escape(theFile.name);
                $scope.NewData.ProfilePicture.ImageType = escape(theFile.type);
                switch (escape(theFile.type)) {
                    // The 'Header string' of image needs to be removed before sending to server
                    // And since content of the Header depends on the image type, we need a Switch Statement to take care of it
                    case 'image/png': // deletes the 'Header String' for 'png' images
                        $scope.NewData.ProfilePicture.ImageContent = (e.target.result + "").replace('data:image/png;base64,', '');
                        break;
                    case 'image/jpeg': // deletes the 'Header String' for 'jpeg' images
                        $scope.NewData.ProfilePicture.ImageContent = (e.target.result + "").replace('data:image/jpeg;base64,', '');
                        break;
                    default:
                        break;
                }
                console.log($scope.NewData.ProfilePicture);
            };
        })(files[0]);
        reader.readAsDataURL(files[0]); // Read in the image file as a data URL.
    };

    $scope.TrimUsername = function (input) {
        if (input) {
            input = input.replace(/ /g, "_");
            $scope.NewData.Username = input.toLowerCase();
        }
        else
            $scope.NewData.Username = "";
    };

    $scope.DirtifyForm = function () {
        var inputElements = document.getElementsByTagName("input");
        var selectElements = document.getElementsByTagName("select");
        for (i = 0; i < inputElements.length; i++)
            inputElements[i].className += " ng-dirty";
        for (i = 0; i < selectElements.length; i++)
            selectElements[i].className += " ng-dirty";
    };

    $scope.Save = function (isValid) {
        if (isValid) {
            $scope.NewData.Token = $scope.Token;
            $scope.NewData.Options = $scope.NewData.Options ? $scope.NewData.Options.map(function (option) {
                return option.Id;
            }) : [];
            SharedServices.Post(passData.url, $scope.NewData).then(function (myResponse) {
                $uibModalInstance.close();
                SharedServices.Alert(myResponse.response.data);
            });
        }
        else
            $scope.DirtifyForm();
    };

    $scope.showErrorMsg = function (formGroup) {
        if (formGroup.$error.required) {
            return "Field is required.";
        }
        else if (formGroup.$error.minlength) {
            return "Field must have at least 3 characters.";
        }
        else if (formGroup.$error.maxlength) {
            return "Field cannot have more than 100 characters.";
        }
        else if (formGroup.$error.min) {
            return "Value must be greater than or equal to 0.";
        }
        else if (formGroup.$error.max) {
            return "Value must be less than or equal to 100.";
        }
    };

    $scope.RefreshFormData = function () {
        $scope.NewData = passData.data;
    };

    $scope.ok = function () {
        $uibModalInstance.close();
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
