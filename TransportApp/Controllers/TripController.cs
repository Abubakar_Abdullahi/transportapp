﻿using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using TransportApp.Hubs;
using TransportApp.Models;
using TransportApp.Security;
using static TransportApp.Controllers.Tools;

namespace TransportApp.Controllers
{
    public class TripController : Controller
    {
        private TransportEntities db = new TransportEntities();

        private List<StopData> RetrieveStops(int TripId)
        {
            Trip trip = db.Trips.Find(TripId);
            List<Stop> list1 = trip.TripPassengers.OrderBy(tp => tp.SortingOrder).Select(tp => tp.Stop).Distinct().ToList();
            list1.RemoveAll(stop => stop == null);
            List<StopData> list2 = list1.Select(stop => new StopData()
            {
                TripId = TripId,
                StopId = stop.StopId,
                Passengers = stop.TripPassengers.Where(tp => tp.TripId == TripId).Select(tp => tp.Passenger).Select(passenger => new Tools.UserData()
                {
                    UserId = passenger.PassengerId,
                    RoleName = passenger.User.Role.RoleName,
                    Name = passenger.User.Name,
                    Surname = passenger.User.Surname,
                    PhoneNo = passenger.User.PhoneNo,
                    Coming = passenger.TripPassengers.SingleOrDefault(tp => tp.TripId == TripId).Coming
                }).ToList(),
                Latitude = stop.Latitude,
                Longitude = stop.Longitude,
                isCurrent = false,
                isEntered = false
            }).ToList();

            int? destinationId = trip.DestinationId;
            int num1;
            if (destinationId.HasValue) {
                destinationId = trip.DestinationId;
                int num2 = 0;
                num1 = destinationId.GetValueOrDefault() == num2 ? (!destinationId.HasValue ? 1 : 0) : 1;
            }
            else
                num1 = 0;
            if (num1 != 0)
            {
                List<StopData> stopDataList = list2;
                StopData stopData = new StopData();
                destinationId = trip.DestinationId;
                stopData.StopId = destinationId.Value;
                stopData.Passengers = new List<UserData>() {
                    new UserData() {
                        Name = trip.DestinationName,
                        Surname = ""
                    }
                };
                stopData.Latitude = trip.Stop.Latitude;
                stopData.Longitude = trip.Stop.Longitude;
                stopData.isCurrent = false;
                stopData.isEntered = false;
                stopDataList.Add(stopData);
            }

            return list2;
        }

        public ActionResult Index()
        {
            var RequestToken = Request.Cookies.Get("Token");
            if (RequestToken != null && JwtManager.ValidateToken(RequestToken.Value, ManagerRole) != 0)
            {
                ViewBag.Name = Request.Cookies.Get("Name").Value;
                return View();
            }
            else
            {
                ViewBag.RedirectLocation = "/Trip";
                return RedirectToAction("Login", "User");
            }
        }

        public ActionResult API()
        {
            var RequestToken = Request.Cookies.Get("Token");
            if (RequestToken != null && JwtManager.ValidateToken(RequestToken.Value, ManagerRole) != 0)
            {
                ViewBag.Name = Request.Cookies.Get("Name").Value;
                return View();
            }
            else
            {
                ViewBag.RedirectLocation = "/Trip/API";
                return RedirectToAction("Login", "User");
            }
        }

        public ActionResult Details()
        {
            var RequestToken = Request.Cookies.Get("Token");
            if (RequestToken != null && JwtManager.ValidateToken(RequestToken.Value, ManagerRole) != 0)
            {
                ViewBag.Name = Request.Cookies.Get("Name").Value;
                return View();
            }
            else
            {
                ViewBag.RedirectLocation = "/Trip";
                return RedirectToAction("Login", "User");
            }
        }

        public JsonResult StartTrip(string serverInput) 
        {
            JsonResult result = new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            StopData trip;
            Trip ChangedTrip;
            DateTime today = DateTime.Today;
            DateTime date1970 = new DateTime(1970, 1, 1, 0, 0, 0);

            try
            {
                trip = JsonConvert.DeserializeObject<StopData>(serverInput);
                if (trip.TripId == 0 || trip.Timestamp == 0)
                    throw new Exception();
            }
            catch
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "The argument passed could not be processed.";
                result.Data = "The argument passed could not be processed.";
                return result;
            }

            if (JwtManager.ValidateToken(trip.Token, DriverRole) == 0)
            {
                Response.StatusCode = 401;
                result.Data = "User does not have access to the resource.";
                return result;
            }

            try
            {
                ChangedTrip = db.Trips.Find(trip.TripId);
                ChangedTrip.Status = true;
                db.SaveChanges();
            }
            catch
            {
                Response.StatusCode = 500;
                Response.StatusDescription = "Trip could not be started.";
                result.Data = "Trip could not be started";
                return result;
            }

            try
            {
                IHubContext hubContext = GlobalHost.ConnectionManager.GetHubContext<MonitorHub>();
                hubContext.Clients.All.RetrieveOngoingTrips(new {
                    ChangedTrip = new
                    {
                        ChangedTrip.TripId,
                        TripName = ChangedTrip.Type + "_" + ChangedTrip.DestinationName,
                        Started = true,
                        ActualStartTime = date1970.AddSeconds(trip.Timestamp).ToShortTimeString()
                    },
                    CurrentTrips = db.Trips.Where(t => t.Status).Select(t => new
                    {
                        t.TripId,
                        t.Type,
                        TripName = t.Type + "_" + t.DestinationName,
                        Status = "Active",
                        t.StartTime,
                        t.EndTime,
                        PassengerCount = t.TripPassengers.Count()
                    })
                });
                
                if (!db.TripReports.Any(tr => tr.TripId == trip.TripId && tr.ActualStartTime.Year == today.Year && tr.ActualStartTime.Month == today.Month && tr.ActualStartTime.Day == today.Day))
                {
                    db.TripReports.Add(new TripReport
                    {
                        TripId = trip.TripId,
                        ActualStartTime = date1970.AddSeconds(trip.Timestamp),
                        ActualEndTime = date1970
                    });
                }

                db.SaveChanges();
            }
            catch
            {
            }
            
            Response.StatusCode = 200;
            return GetTripDetails(JsonConvert.SerializeObject(trip));
        }

        public JsonResult EndTrip(string serverInput)
        {
            JsonResult result = new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            StopData trip;
            Trip ChangedTrip;

            try
            {
                trip = JsonConvert.DeserializeObject<StopData>(serverInput);
                if (trip.TripId == 0)
                    throw new Exception();
            }
            catch
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "The argument passed could not be processed.";
                result.Data = "The argument passed could not be processed.";
                return result;
            }

            if (JwtManager.ValidateToken(trip.Token, DriverRole) == 0)
            {
                Response.StatusCode = 401;
                result.Data = "User does not have access to the resource.";
                return result;
            }

            try
            {
                foreach (TripPassenger tripPassenger in db.TripPassengers.Where(tp => tp.TripId == trip.TripId))
                    tripPassenger.Coming = true;

                ChangedTrip = db.Trips.Find(trip.TripId);
                ChangedTrip.Status = false;

                db.SaveChanges();
                result.Data = "Trip has ended successfully.";
            }
            catch
            {
                Response.StatusCode = 500;
                Response.StatusDescription = "Trip could not be ended.";
                result.Data = "Trip could not be ended";
                return result;
            }

            try
            {
                IHubContext hubContext = GlobalHost.ConnectionManager.GetHubContext<MonitorHub>();
                hubContext.Clients.All.RetrieveOngoingTrips(new
                {
                    ChangedTrip = new
                    {
                        ChangedTrip.TripId,
                        TripName = ChangedTrip.Type + "_" + ChangedTrip.DestinationName,
                        Started = false
                    },
                    CurrentTrips = db.Trips.Where(t => t.Status).Select(t => new
                    {
                        t.TripId,
                        t.Type,
                        TripName = t.Type + "_" + t.DestinationName,
                        Status = "Active",
                        t.StartTime,
                        t.EndTime,
                        PassengerCount = t.TripPassengers.Count()
                    })
                });

                DateTime today = DateTime.Today.Date;
                db.TripReports.SingleOrDefault(tr => tr.TripId == trip.TripId && tr.ActualStartTime.Year == today.Year && tr.ActualStartTime.Month == today.Month && tr.ActualStartTime.Day == today.Day).ActualEndTime = new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(trip.Timestamp);
                db.SaveChanges();
            }
            catch
            {
            }

            Response.StatusCode = 200;
            return result;
        }

        // [GenericAuthorizationAttribute]
        public JsonResult GetTrips(string serverInput)
        {
            JsonResult result = new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            DateTime date1970 = new DateTime(1970, 1, 1, 3, 0, 0);
            UserData userData;

            try
            {
                userData = JsonConvert.DeserializeObject<UserData>(serverInput);
            }
            catch
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "The argument passed could not be processed.";
                result.Data = "The argument passed could not be processed.";
                return result;
            }

            int UserId = JwtManager.ValidateToken(userData.Token, AllRoles);
            if (UserId == 0)
            {
                Response.StatusCode = 401;
                result.Data = "User does not have access to the resource.";
                return result;
            }

            try
            {
                User user = db.Users.Find(UserId);
                string roleName = user.Role.RoleName;
                List<TripInfo> tripList;

                switch(user.Role.RoleName)
                {
                    case "Manager":
                        tripList = db.Trips.Select(trip => new TripInfo()
                        {
                            TripId = trip.TripId,
                            TripName = trip.Type + "Trip_" + trip.DestinationName,
                            Type = trip.Type,
                            DestinationName = trip.DestinationName,
                            StartTimestamp = trip.StartTime,
                            EndTimestamp = trip.EndTime,
                            StopCount = trip.TripPassengers.Select(tp => tp.StopId).Distinct().Count(stopId => stopId.HasValue),
                            PassengerCount = trip.TripPassengers.Count
                        }).ToList();
                        break;
                    case "Driver":
                        tripList = user.Driver.Trips.Select(trip => new TripInfo()
                        {
                            TripId = trip.TripId,
                            TripName = trip.Type + "Trip_" + trip.DestinationName,
                            Type = trip.Type,
                            StartTimestamp = trip.StartTime,
                            EndTimestamp = trip.EndTime,
                            StopCount = trip.TripPassengers.Select(tp => tp.StopId).Distinct().Count(stopId => stopId.HasValue),
                            PassengerCount = trip.TripPassengers.Count
                        }).ToList();
                        break;
                    case "Passenger":
                        tripList = user.Passenger.TripPassengers.Select(tp => tp.Trip).Select(trip => new TripInfo()
                        {
                            TripId = trip.TripId,
                            TripName = trip.Type + "Trip_" + trip.DestinationName,
                            Type = trip.Type,
                            StartTime = date1970.AddSeconds((double)trip.StartTime).ToShortTimeString(),
                            EndTime = date1970.AddSeconds((double)trip.EndTime).ToShortTimeString()
                        }).ToList();
                        break;
                    default:
                        throw new Exception();
                }
                
                foreach (var trip in tripList)
                {   
                    trip.StartTime = date1970.AddSeconds(trip.StartTimestamp).ToShortTimeString();
                    trip.EndTime = date1970.AddSeconds(trip.EndTimestamp).ToShortTimeString();
                }

                result.Data = tripList;
            }
            catch
            {
                Response.StatusCode = 500;
                Response.StatusDescription = "Trips could not be retrieved.";
                result.Data = "Trips could not be retrieved.";
                return result;
            }

            return result;
        }

        public JsonResult GetTripDetails(string serverInput)
        {
            JsonResult result = new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            TripData tripData;
            try
            {
                tripData = JsonConvert.DeserializeObject<Tools.TripData>(serverInput);
            }
            catch
            {
                this.Response.StatusCode = 400;
                this.Response.StatusDescription = "The argument passed could not be processed.";
                result.Data = (object)"The argument passed could not be processed.";
                return result;
            }
            if (JwtManager.ValidateToken(tripData.Token, Tools.ManagerDriver) == 0)
            {
                this.Response.StatusCode = 401;
                result.Data = (object)"User does not have access to the resource.";
                return result;
            }
            Trip TripData = db.Trips.Find(tripData.TripId);
            if (TripData != null)
            {
                try
                {
                    TripDetails tripDetails = new TripDetails
                    {
                        Type = TripData.Type,
                        Stops = TripData.TripPassengers.OrderBy(tp => tp.SortingOrder).Select(tp => tp.Stop).Distinct().Where(stop => stop != null).Select(stop => new Tools.StopData()
                        {
                            StopId = stop.StopId,
                            Passengers = stop.TripPassengers.Select<TripPassenger, Passenger>((Func<TripPassenger, Passenger>)(tp => tp.Passenger)).Distinct<Passenger>().Select<Passenger, Tools.UserData>((Func<Passenger, Tools.UserData>)(passenger => new Tools.UserData()
                            {
                                UserId = passenger.PassengerId,
                                RoleName = passenger.User.Role.RoleName,
                                Name = passenger.User.Name,
                                Surname = passenger.User.Surname,
                                PhoneNo = passenger.User.PhoneNo,
                                Coming = passenger.TripPassengers.SingleOrDefault<TripPassenger>((Func<TripPassenger, bool>)(tp => tp.TripId == TripData.TripId)).Coming
                            })).ToList<Tools.UserData>(),
                            Latitude = stop.Latitude,
                            Longitude = stop.Longitude,
                            isCurrent = false,
                            isEntered = false
                        }).ToList(),
                        Passengers = TripData.TripPassengers.Count != 0 ? TripData.TripPassengers.Select(tp => new
                        {
                            tp.PassengerId,
                            PassengerName = tp.Passenger.User.Name + " " + tp.Passenger.User.Surname
                        }).ToDictionary(passenger => passenger.PassengerId.ToString(), passenger => passenger.PassengerName) : null,
                        Drivers = TripData.Drivers.Count != 0 ? TripData.Drivers.Select(driver => new UserData
                        {
                            UserId = driver.DriverId,
                            RoleName = driver.User.Role.RoleName,
                            Name = driver.User.Name,
                            Surname = driver.User.Surname,
                            PhoneNo = driver.User.PhoneNo
                        }).ToDictionary(driver => driver.UserId.ToString()) : null,
                        AttendanceList = TripData.TripPassengers.Count != 0 ? TripData.TripPassengers.Select(tp => new Tools.Attendance()
                        {
                            PassengerId = tp.PassengerId,
                            Name = tp.Passenger.User.Name,
                            Surname = tp.Passenger.User.Surname,
                            Coming = new bool?(tp.Coming),
                            BeaconId = tp.Passenger.BeaconId,
                            isEngaged = false
                        }).ToDictionary(stop => stop.BeaconId) : null,
                        Destination = TripData.DestinationId.HasValue ? new Destination
                        {
                            DestinationName = TripData.DestinationName,
                            Latitude = TripData.Stop.Latitude,
                            Longitude = TripData.Stop.Longitude,
                            StopId = (int) TripData.DestinationId
                        } : null
                    };
                    
                    if(tripDetails.Destination != null)
                    {
                        var destination = (Destination)tripDetails.Destination;
                        tripDetails.Stops.Add(new StopData
                        {
                            StopId = destination.StopId,
                            Latitude = destination.Latitude,
                            Longitude = destination.Longitude,
                            Passengers = new List<UserData>
                            {
                                new UserData
                                {
                                    Name = destination.DestinationName,
                                    Surname = ""
                                }
                            }
                        });
                    }
                    
                    result.Data = tripDetails;
                }
                catch
                {
                    Response.StatusCode = 500;
                    Response.StatusDescription = "Details of the Trip could not be retrieved.";
                    result.Data = (object)"Details of the Trip could not be retrieved.";
                }
            }
            else
            {
                this.Response.StatusCode = 404;
                this.Response.StatusDescription = "The Trip with the provided TripId could not be found";
                result.Data = (object)"The Trip with the provided TripId could not be found.";
            }
            return result;
        }

        public JsonResult SaveStop(string serverInput)
        {
            JsonResult result = new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            StopData stopData;
            TripPassenger tempTp;

            try
            {
                stopData = JsonConvert.DeserializeObject<StopData>(serverInput);
                if (stopData.TripId == 0)
                    throw new Exception();
            }
            catch
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "The argument passed could not be processed or TripId is 0.";
                result.Data = "The argument passed could not be processed or TripId is 0.";
                return result;
            }

            int UserId = JwtManager.ValidateToken(stopData.Token, AllRoles);
            if (UserId == 0)
            {
                Response.StatusCode = 401;
                result.Data = "User does not have access to the resource.";
                return result;
            }

            if(stopData.StopId == 0)
            {
                try
                {
                    foreach (int passengerId in stopData.PassengersId)
                    {
                        tempTp = db.TripPassengers.SingleOrDefault(tp => tp.TripId == stopData.TripId && tp.PassengerId == passengerId);
                        if (tempTp != null)
                        {
                            tempTp.Stop = new Stop
                            {
                                Latitude = stopData.Latitude,
                                Longitude = stopData.Longitude,
                                Timestamp = stopData.Timestamp
                            };
                            tempTp.AssignerId = UserId;
                        }
                    }

                    db.SaveChanges();
                    result.Data = RetrieveStops(stopData.TripId);
                }
                catch
                {
                    Response.StatusCode = 500;
                    Response.StatusDescription = "Stop could not be added.";
                    result.Data = "Stop could not be added.";
                }
            }
            else
            {
                try
                {
                    var tripStops = db.TripPassengers.Where(tp => tp.TripId == stopData.TripId && tp.StopId == stopData.StopId);

                    foreach (var tripStop in tripStops)
                    {
                        if (!stopData.PassengersId.Contains(tripStop.PassengerId))
                            tripStop.StopId = null;
                    }

                    foreach (int passengerId in stopData.PassengersId)
                    {
                        tempTp = db.TripPassengers.SingleOrDefault(tp => tp.TripId == stopData.TripId && tp.PassengerId == passengerId);
                        if (tempTp != null)
                        {
                            tempTp.StopId = stopData.StopId;
                            //tempTp.Stop.Latitude = stopData.Latitude != 0 ? stopData.Latitude : tempTp.Stop.Latitude;
                            //tempTp.Stop.Longitude = stopData.Longitude != 0 ? stopData.Longitude : tempTp.Stop.Longitude;
                            tempTp.AssignerId = UserId;
                        }   
                    }

                    db.SaveChanges();
                    result.Data = RetrieveStops(stopData.TripId);
                }
                catch
                {
                    Response.StatusCode = 500;
                    Response.StatusDescription = "Stop could not be edited.";
                    result.Data = "Stop could not be edited.";
                }
            }

            return result;
        }

        public JsonResult SaveTripDestination(string serverInput)
        {
            JsonResult result = new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            Destination destination;

            try
            {
                destination = JsonConvert.DeserializeObject<Destination>(serverInput);
                if (destination.TripId == 0)
                    throw new Exception();
            }
            catch
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "The argument passed could not be processed or TripId is 0.";
                result.Data = "The argument passed could not be processed or TripId is 0.";
                return result;
            }

            if (JwtManager.ValidateToken(destination.Token, DriverRole) == 0)
            {
                Response.StatusCode = 401;
                result.Data = "User does not have access to the resource.";
                return result;
            }

            Trip trip = db.Trips.Find(destination.TripId);
            if (trip == null)
            {
                Response.StatusCode = 404;
                Response.StatusDescription = "The Trip with the provided TripId could not be found";
                result.Data = "The Trip with the provided TripId could not be found.";
            }
            else
            {
                try
                {
                    trip.DestinationName = destination.DestinationName == null || !(destination.DestinationName != "") ? trip.DestinationName : destination.DestinationName;
                    trip.Stop = new Stop()
                    {
                        StopId = destination.StopId,
                        Latitude = destination.Latitude,
                        Longitude = destination.Longitude,
                        Timestamp = destination.Timestamp
                    };
                    db.SaveChanges();
                    result.Data = "Destination for the Trip has been saved successfully.";
                }
                catch
                {
                    Response.StatusCode = 500;
                    Response.StatusDescription = "Destination could not be saved.";
                    result.Data = "Destination could not be saved.";
                }
            }
            return result;
        }

        public JsonResult OrderStops(string serverInput)
        {
            JsonResult jsonResult = new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            Tools.SortingData sortingData;
            try
            {
                sortingData = JsonConvert.DeserializeObject<Tools.SortingData>(serverInput);
            }
            catch
            {
                this.Response.StatusCode = 400;
                this.Response.StatusDescription = "The argument passed could not be processed.";
                jsonResult.Data = (object)"The argument passed could not be processed.";
                return jsonResult;
            }
            if (JwtManager.ValidateToken(sortingData.Token, Tools.DriverRole) == 0)
            {
                this.Response.StatusCode = 401;
                jsonResult.Data = (object)"User does not have access to the resource.";
                return jsonResult;
            }
            try
            {
                int? destinationId = this.db.Trips.Find((object)sortingData.TripId).DestinationId;
                foreach (KeyValuePair<int, int> keyValuePair in (IEnumerable<KeyValuePair<int, int>>)sortingData.StopsOrder)
                {
                    KeyValuePair<int, int> stopOrder = keyValuePair;
                    TripPassenger tripPassenger = this.db.TripPassengers.SingleOrDefault<TripPassenger>((Expression<Func<TripPassenger, bool>>)(tp => tp.TripId == sortingData.TripId && tp.StopId == (int?)stopOrder.Key));
                    int key = stopOrder.Key;
                    int? nullable = destinationId;
                    int valueOrDefault = nullable.GetValueOrDefault();
                    if ((key == valueOrDefault ? (nullable.HasValue ? 1 : 0) : 0) == 0 && tripPassenger != null)
                        tripPassenger.SortingOrder = stopOrder.Value;
                }
                this.db.SaveChanges();
            }
            catch
            {
                this.Response.StatusCode = 500;
                this.Response.StatusDescription = "Stops could not be sorted.";
                jsonResult.Data = (object)"Stops could not be sorted.";
            }
            try
            {
                jsonResult.Data = (object)this.RetrieveStops(sortingData.TripId);
            }
            catch
            {
                this.Response.StatusCode = 204;
                jsonResult.Data = (object)"Stops have successfully been sorted. But the sorted Stops could not be retrived from databse.";
            }
            return jsonResult;
        }

        public JsonResult SetAbsent(string serverInput)
        {
            JsonResult jsonResult = new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            Tools.AbsenceData absenceData;
            try
            {
                absenceData = JsonConvert.DeserializeObject<Tools.AbsenceData>(serverInput);
                if (absenceData.TripId == 0 || absenceData.PassengerId == 0)
                    throw new Exception();
            }
            catch
            {
                this.Response.StatusCode = 400;
                this.Response.StatusDescription = "The argument passed could not be processed.";
                jsonResult.Data = (object)"The argument passed could not be processed.";
                return jsonResult;
            }
            if (JwtManager.ValidateToken(absenceData.Token, Tools.PassengerRole) == 0)
            {
                this.Response.StatusCode = 401;
                jsonResult.Data = (object)"User does not have access to the resource.";
                return jsonResult;
            }
            TripPassenger tripPassenger = this.db.TripPassengers.Find((object)absenceData.TripId, (object)absenceData.PassengerId);
            if (tripPassenger == null)
            {
                this.Response.StatusCode = 404;
                this.Response.StatusDescription = "The Passenger is not included in this trip.";
                jsonResult.Data = (object)"The Passenger is not included in this trip.";
                return jsonResult;
            }
            try
            {
                tripPassenger.Coming = false;
                this.db.SaveChanges();
                jsonResult.Data = (object)"Passenger has successfully been marked absent for the next Trip.";
            }
            catch
            {
                this.Response.StatusCode = 500;
                this.Response.StatusDescription = "Passenger could not be marked absent.";
                jsonResult.Data = (object)"Passenger could not be marked absent.";
            }
            try
            {
                NotificationController.Notify(new Tools.NotificationData()
                {
                    NotificationType = "Absent",
                    TripId = absenceData.TripId,
                    ReceiversId = tripPassenger.Trip.Drivers.Select<Driver, int>((Func<Driver, int>)(driver => driver.DriverId)).ToArray<int>(),
                    SenderId = absenceData.PassengerId
                });
            }
            catch
            {
                this.Response.StatusCode = 200;
            }
            return jsonResult;
        }

        public JsonResult DeleteStop(string serverInput)
        {
            JsonResult result = new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            StopData stopData;

            try
            {
                stopData = JsonConvert.DeserializeObject<StopData>(serverInput);
            }
            catch
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "The argument passed could not be processed.";
                result.Data = "The argument passed could not be processed.";
                return result;
            }
            if (JwtManager.ValidateToken(stopData.Token, DriverPassenger) == 0)
            {
                Response.StatusCode = 401;
                result.Data = "User does not have access to the resource.";
                return result;
            }
            try
            {
                Stop chosenStop = db.Stops.Find(stopData.StopId);
                if (chosenStop == null)
                {
                    Response.StatusCode = 404;
                    Response.StatusDescription = "Stop does not exist in the database.";
                    result.Data = "Stop does not exist in the database.";
                    return result;
                }

                chosenStop.TripPassengers = chosenStop.TripPassengers.Where(tp => tp.TripId != stopData.TripId).ToList();
                if (!chosenStop.TripPassengers.Any())
                    db.Stops.Remove(chosenStop);
                db.SaveChanges();

                result.Data = RetrieveStops(stopData.TripId);
            }
            catch
            {
                Response.StatusCode = 500;
                result.Data = "Stop could not be deleted.";
            }

            return result;
        }
    }
}
