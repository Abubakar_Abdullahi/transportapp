﻿
TransportApp.controller('TripCtrl', ['$scope', 'SharedServices', function ($scope, SharedServices) {

    $scope.Trips;
    $scope.TripPath;
    $scope.Drivers;
    $scope.Token;
    
    $scope.init = function () {
        $scope.Token = Cookies.get("Token");
        $scope.GetTrips();
        $scope.GetDrivers();
    };

    $scope.initMap = function () {
        var map = new google.maps.Map(document.getElementById('trip-path-map'), {
            zoom: 13,
            center: { lat: $scope.TripPath.Stops[0].lat, lng: $scope.TripPath.Stops[0].lng }
        });

        var infowindow = new google.maps.InfoWindow();
        var marker, i;

        for (i = 0; i < $scope.TripPath.Stops.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng($scope.TripPath.Stops[i].lat, $scope.TripPath.Stops[i].lng),
                map: map,
                icon: "../../icons/map-pin-pink.png"
            });

            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent($scope.TripPath.Stops[i].label);
                    infowindow.open(map, marker);
                };
            })(marker, i));
        }

        var flightPath = new google.maps.Polyline({
            path: $scope.TripPath.Stops,
            geodesic: true,
            strokeColor: '#424242',
            strokeOpacity: 1.0,
            strokeWeight: 2
        });
        flightPath.setMap(map);
    };

    $scope.GetTrips = function () {
        SharedServices.Post('/Trip/GetTrips', { UserId: 0, Token: $scope.Token }).then(function (myResponse) {
            if (myResponse.IsSuccessful) {
                $scope.Trips = myResponse.response.data;
                $scope.GetTripPath($scope.Trips[0]);
            }   
            else
                console.log("An error occurred while getting Trips.");
        });
    };

    $scope.GetTripPath = function (chosenTrip) {
        SharedServices.Post('/TripBackend/GetTripPath', { TripId: chosenTrip.TripId, Token: $scope.Token }).then(function (myResponse) {
            if (myResponse.IsSuccessful) {
                $scope.TripPath = myResponse.response.data;
                $scope.TripPath.Stops.push($scope.TripPath.Destination);
                $scope.initMap();
                console.log($scope.TripPath);
            }
            else
                console.log("An error occurred while getting Trip's path.");
        });
    };

    $scope.GetDrivers = function () {
        SharedServices.Post('/Driver/GetDriversToday', { Token: $scope.Token}).then(function (myResponse) {
            if (myResponse.IsSuccessful) {
                $scope.Drivers = myResponse.response.data;
            }   
            else
                console.log("An error occurred while getting Drivers.");
        });
    };

    $scope.GetAllPassengers = function () {
        SharedServices.Post('/Passenger/GetPassengers', $scope.Token).then(function (myResponse) {
            if (myResponse.IsSuccessful) {
                $scope.AllPassengers = myResponse.response.data;

                var data = {
                    SelectedOptions: $scope.Trip.Passengers ? $scope.Passengers.map(function (passenger) {
                        return {
                            Id: passenger.UserId,
                            Label: passenger.Name
                        };
                    }) : null,
                    AllOptions: $scope.AllPassengers.map(function (passenger) {
                        return {
                            Id: passenger.PassengerId,
                            Label: passenger.Name
                        };
                    })
                };
                $scope.SelectOptions(data);
            }
            else
                console.log("An error occurred while getting Passengers.");
        });
    };

    $scope.SelectOptions = function (data) {
        SharedServices.OpenModal('../Views/Shared/Options.html', 'OptionsCtrl', 'md', data).result.then(function (selectedOptions) {
            $scope.Trip.Passengers = [];
            for (i = 0; i < selectedOptions.length; i++)
                $scope.Passengers[i] = {
                    UserId: selectedOptions[i].Id,
                    Name: selectedOptions[i].Label
                };
        });
    };

    $scope.OpenForm = function () {
        var data = { AllDataUrl: '/Passenger/GetPassengers' };
        SharedServices.OpenModal("../Views/Trip/TripForm.html", "FormCtrl", "lg", data, true, "/TripBackend/SaveTrip", null, $scope.Drivers).result.then(function () {
            $scope.GetTrips();
        });
    };

    $scope.GoToTripDetails = function (chosenTrip) {
        var tripWindow = window.open('/Trip/Details', '_self');
        tripWindow.passData = {
            Trip: chosenTrip,
            Drivers: $scope.Drivers
        };
        tripWindow.sessionStorage.passData = JSON.stringify({
            Trip: chosenTrip,
            Drivers: $scope.Drivers
        });
    };

    $scope.DeleteConfirmation = function (tripId) {
        var data = {
            TripId: tripId,
            Token: $scope.Token
        };
        SharedServices.OpenModal("../Views/Shared/ConfirmDelete.html", "DeleteItemCtrl", "md", data, null, "/TripBackend/DeleteTrip").result.then(function () {
            $scope.GetTrips();
        });
    };
}]);