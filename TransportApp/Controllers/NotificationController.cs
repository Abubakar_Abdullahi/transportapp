﻿// Decompiled with JetBrains decompiler
// Type: TransportApp.Controllers.NotificationController
// Assembly: TransportApp, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9FC5505E-8A04-49A5-ADEF-D6AFF964F1CE
// Assembly location: C:\Users\Abubakar\Desktop\pfts\bin\TransportApp.dll

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Text;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TransportApp.Models;
using TransportApp.Security;
using static TransportApp.Controllers.Tools;

namespace TransportApp.Controllers
{
    public class NotificationController : Controller
    {
        private TransportEntities db = new TransportEntities();

        public ActionResult Index()
        {
            var RequestToken = Request.Cookies.Get("Token");
            if (RequestToken != null && JwtManager.ValidateToken(RequestToken.Value, ManagerRole) != 0)
            {
                ViewBag.Name = Request.Cookies.Get("Name").Value;
                return View();
            }
            else
            {
                ViewBag.RedirectLocation = "/Notification";
                return RedirectToAction("Login", "User");
            }
        }

        private object GetUserNotifications(int userId)
        {
            return db.Notifications.Where(n => n.Type != "GeofenceEntry" && (n.SenderId == userId || n.Users.Select(user => user.UserId).Contains(userId))).Select(notification => new
            {
                Sender = new
                {
                    Name = notification.User.Name + " " + notification.User.Surname,
                    Role = notification.User.Role.RoleName
                },
                Receivers = notification.Users.Select(receiver => new
                {
                    Name = receiver.Name + " " + receiver.Surname,
                    Role = receiver.Role.RoleName
                }),
                Datetime = notification.DateTime,
                Message = notification.Type == "Delay" ? "Your trip will be delayed for " + notification.Delay + " minutes." : (notification.Type == "Absent" ? notification.User.Name + " " + notification.User.Surname + " will not join the next trip." : "Your trip has been cancelled for today")
            }).ToList();
        }

        public JsonResult Notify(string serverInput)
        {
            JsonResult jsonResult = new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            Tools.NotificationData notificationData;

            try
            {
                notificationData = JsonConvert.DeserializeObject<Tools.NotificationData>(serverInput);
            }
            catch
            {
                this.Response.StatusCode = 400;
                this.Response.StatusDescription = "Arguement passed could not be processed.";
                jsonResult.Data = (object)"Arguement passed could not be processed.";
                return jsonResult;
            }

            if (JwtManager.ValidateToken(notificationData.Token, Tools.AllRoles) == 0)
            {
                this.Response.StatusCode = 401;
                jsonResult.Data = (object)"User does not have access to the resource.";
                return jsonResult;
            }

            switch (NotificationController.Notify(notificationData))
            {
                case 1:
                    this.Response.StatusCode = 400;
                    this.Response.StatusDescription = "Arguement passed could not be processed. NotificationType field should be Delay, GeofenceEntry or TripCancellation.";
                    jsonResult.Data = (object)"Arguement passed could not be processed. NotificationType field should be Delay, GeofenceEntry or TripCancellation.";
                    break;
                case 2:
                    this.Response.StatusCode = 200;
                    jsonResult.Data = (object)"Notification has been sent successfully.";
                    break;
                case 3:
                    this.Response.StatusCode = 500;
                    this.Response.StatusDescription = "Notification could not be sent.";
                    jsonResult.Data = (object)"Notification could not be sent.";
                    break;
            }
            return jsonResult;
        }

        public static int Notify(NotificationData notificationData)
        {
            TransportEntities transportEntities = new TransportEntities();
            string notificationType = notificationData.NotificationType;
            string str1;
            if (!(notificationType == "Delay"))
            {
                if (!(notificationType == "GeofenceEntry"))
                {
                    if (!(notificationType == "TripCancellation"))
                    {
                        if (!(notificationType == "Absent"))
                            return 1;
                        User user = transportEntities.Users.Find((object)notificationData.SenderId);
                        str1 = user.Name + " " + user.Surname + " will not join the next trip.";
                    }
                    else
                        str1 = "Your trip has been cancelled for today.";
                }
                else
                    str1 = "get ready, the bus is approaching your house.";
            }
            else
                str1 = "Your trip will be delayed for " + (object)notificationData.Delay + " minutes.";
            try
            {
                string[] array = transportEntities.Users.Where<User>((Expression<Func<User, bool>>)(user => notificationData.ReceiversId.Contains<int>(user.UserId))).Select<User, string>((Expression<Func<User, string>>)(user => user.DeviceId)).ToArray<string>();
                HttpWebRequest httpWebRequest = WebRequest.Create("https://onesignal.com/api/v1/notifications") as HttpWebRequest;
                httpWebRequest.KeepAlive = true;
                httpWebRequest.Method = "POST";
                httpWebRequest.ContentType = "application/json; charset=utf-8";
                httpWebRequest.Headers.Add("authorization", OneSignalHeaderValue);
                JavaScriptSerializer scriptSerializer = new JavaScriptSerializer();
                var data = new
                {
                    app_id = OneSignalAppId,
                    contents = new { en = str1 },
                    include_player_ids = array
                };
                byte[] bytes = Encoding.UTF8.GetBytes(scriptSerializer.Serialize((object)data));
                string str2 = (string)null;
                try
                {
                    using (Stream requestStream = httpWebRequest.GetRequestStream())
                        requestStream.Write(bytes, 0, bytes.Length);
                    using (HttpWebResponse response = httpWebRequest.GetResponse() as HttpWebResponse)
                    {
                        using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
                            str2 = streamReader.ReadToEnd();
                    }
                }
                catch (WebException ex)
                {
                    Debug.WriteLine(ex.Message);
                    Debug.WriteLine(new StreamReader(ex.Response.GetResponseStream()).ReadToEnd());
                }
                try
                {
                    DbSet<Notification> notifications = transportEntities.Notifications;
                    Notification notification = new Notification();
                    notification.SenderId = notificationData.SenderId;
                    notification.Users = (ICollection<User>)transportEntities.Users.Where<User>((Expression<Func<User, bool>>)(user => notificationData.ReceiversId.Contains<int>(user.UserId))).ToList<User>();
                    notification.Type = notificationData.NotificationType;
                    notification.Delay = new int?(notificationData.Delay);
                    notification.DateTime = (int)DateTimeOffset.UtcNow.ToUnixTimeSeconds();
                    Notification entity = notification;
                    notifications.Add(entity);
                    transportEntities.SaveChanges();
                }
                catch
                {
                }
                return 2;
            }
            catch
            {
                return 3;
            }
        }

        public JsonResult GetNotifications()
        {
            JsonResult jsonResult = new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            try
            {
                jsonResult.Data = (object)this.db.Notifications.Where<Notification>((Expression<Func<Notification, bool>>)(n => n.Type != "GeofenceEntry")).Select(notification => new
                {
                    Sender = new
                    {
                        Name = notification.User.Name + " " + notification.User.Surname,
                        Role = notification.User.Role.RoleName
                    },
                    Receivers = notification.Users.Select(receiver => new
                    {
                        Name = receiver.Name + " " + receiver.Surname,
                        Role = receiver.Role.RoleName
                    }),
                    Datetime = notification.DateTime,
                    Message = notification.Type == "Delay" ? "Your trip will be delayed for " + (object)notification.Delay + " minutes." : (notification.Type == "Absent" ? notification.User.Name + " " + notification.User.Surname + " will not join the next trip." : "Your trip has been cancelled for today")
                }).ToList();
            }
            catch
            {
                this.Response.StatusCode = 500;
                this.Response.StatusDescription = "Notifications could not be retrieved.";
                jsonResult.Data = (object)"Notifications could not be retrieved.";
            }
            return jsonResult;
        }

        public JsonResult GetNotificationsToday()
        {
            JsonResult jsonResult = new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            try
            {
                DateTime dateTime = DateTime.Now;
                dateTime = dateTime.Date;
                int TimestampToday = (int)dateTime.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
                jsonResult.Data = (object)this.db.Notifications.Where<Notification>((Expression<Func<Notification, bool>>)(n => n.Type != "GeofenceEntry" && Math.Floor((double)n.DateTime / 86400.0) * 86400.0 == (double)TimestampToday)).Select(notification => new
                {
                    Sender = new
                    {
                        Name = notification.User.Name + " " + notification.User.Surname,
                        Role = notification.User.Role.RoleName
                    },
                    Receivers = notification.Users.Select(receiver => new
                    {
                        Name = receiver.Name + " " + receiver.Surname,
                        Role = receiver.Role.RoleName
                    }),
                    Datetime = notification.DateTime,
                    Message = notification.Type == "Delay" ? "Your trip will be delayed for " + (object)notification.Delay + " minutes." : (notification.Type == "Absent" ? notification.User.Name + " " + notification.User.Surname + " will not join the next trip." : "Your trip has been cancelled for today")
                }).ToList();
            }
            catch
            {
                this.Response.StatusCode = 500;
                this.Response.StatusDescription = "Notifications could not be retrieved.";
                jsonResult.Data = (object)"Notifications could not be retrieved.";
            }
            return jsonResult;
        }

        public JsonResult GetDriverNotifications(string serverInput)
        {
            JsonResult jsonResult = new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            Tools.DeviceData deviceData;
            try
            {
                deviceData = JsonConvert.DeserializeObject<Tools.DeviceData>(serverInput);
            }
            catch
            {
                this.Response.StatusCode = 400;
                this.Response.StatusDescription = "Arguement passed could not be processed.";
                jsonResult.Data = (object)"Arguement passed could not be processed.";
                return jsonResult;
            }
            if (JwtManager.ValidateToken(deviceData.Token, Tools.ManagerRole) == 0)
            {
                this.Response.StatusCode = 401;
                jsonResult.Data = (object)"User does not have access to the resource.";
                return jsonResult;
            }
            try
            {
                if (deviceData.UserId == 0)
                    jsonResult.Data = (object)this.db.Notifications.Where<Notification>((Expression<Func<Notification, bool>>)(n => n.Type != "GeofenceEntry" && n.User.Role.RoleName == "Driver")).Select(notification => new
                    {
                        Sender = new
                        {
                            Name = notification.User.Name + " " + notification.User.Surname,
                            Role = notification.User.Role.RoleName
                        },
                        Receivers = notification.Users.Select(receiver => new
                        {
                            Name = receiver.Name + " " + receiver.Surname,
                            Role = receiver.Role.RoleName
                        }),
                        Datetime = notification.DateTime,
                        Message = notification.Type == "Delay" ? "Your trip will be delayed for " + (object)notification.Delay + " minutes." : (notification.Type == "Absent" ? notification.User.Name + " " + notification.User.Surname + " will not join the next trip." : "Your trip has been cancelled for today")
                    }).ToList();
                else
                    jsonResult.Data = this.GetUserNotifications(deviceData.UserId);
            }
            catch
            {
                this.Response.StatusCode = 500;
                this.Response.StatusDescription = "Notifications could not be retrieved.";
                jsonResult.Data = (object)"Notifications could not be retrieved.";
            }
            return jsonResult;
        }

        public JsonResult GetPassengerNotifications(string serverInput)
        {
            JsonResult jsonResult = new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            Tools.DeviceData deviceData;
            try
            {
                deviceData = JsonConvert.DeserializeObject<Tools.DeviceData>(serverInput);
            }
            catch
            {
                this.Response.StatusCode = 400;
                this.Response.StatusDescription = "Arguement passed could not be processed.";
                jsonResult.Data = (object)"Arguement passed could not be processed.";
                return jsonResult;
            }
            if (JwtManager.ValidateToken(deviceData.Token, Tools.ManagerRole) == 0)
            {
                this.Response.StatusCode = 401;
                jsonResult.Data = (object)"User does not have access to the resource.";
                return jsonResult;
            }
            try
            {
                if (deviceData.UserId == 0)
                    jsonResult.Data = (object)this.db.Notifications.Where<Notification>((Expression<Func<Notification, bool>>)(n => n.Type != "GeofenceEntry" && n.User.Role.RoleName == "Passenger")).Select(notification => new
                    {
                        Sender = new
                        {
                            Name = notification.User.Name + " " + notification.User.Surname,
                            Role = notification.User.Role.RoleName
                        },
                        Receivers = notification.Users.Select(receiver => new
                        {
                            Name = receiver.Name + " " + receiver.Surname,
                            Role = receiver.Role.RoleName
                        }),
                        Datetime = notification.DateTime,
                        Message = notification.Type == "Delay" ? "Your trip will be delayed for " + (object)notification.Delay + " minutes." : (notification.Type == "Absent" ? notification.User.Name + " " + notification.User.Surname + " will not join the next trip." : "Your trip has been cancelled for today")
                    }).ToList();
                else
                    jsonResult.Data = this.GetUserNotifications(deviceData.UserId);
            }
            catch
            {
                this.Response.StatusCode = 500;
                this.Response.StatusDescription = "Notifications could not be retrieved.";
                jsonResult.Data = (object)"Notifications could not be retrieved.";
            }
            return jsonResult;
        }
    }
}
