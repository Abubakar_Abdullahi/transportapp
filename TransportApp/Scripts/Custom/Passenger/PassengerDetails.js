﻿
TransportApp.controller('PassengerDetailsCtrl', ['$scope', 'SharedServices', function ($scope, SharedServices) {

    $scope.PassData;
    $scope.Passenger;
    $scope.Trips;
    $scope.AllTrips;
    $scope.Notifications;
    $scope.Roles;
    $scope.Token;

    $scope.init = function () {
        $scope.Token = Cookies.get("Token");
        $scope.PassData = window.passData || JSON.parse(window.sessionStorage.passData);
        $scope.Trips = $scope.PassData.Trips;
        $scope.GetPassengerDetails($scope.PassData.Passenger.PassengerId);
        $scope.GetNotifications($scope.PassData.Passenger.PassengerId);
        $scope.GetRoles();
    };

    $scope.initMap = function (centerCoord) {
        $scope.Map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            center: typeof centerCoord !== undefined ? centerCoord : { lat: $scope.TripPath.Path[0].lat, lng: $scope.TripPath.Path[0].lng }
        });

        var infowindow = new google.maps.InfoWindow();
        var marker, i;

        for (i = 0; i < $scope.TripPath.Stops.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng($scope.TripPath.Stops[i].lat, $scope.TripPath.Stops[i].lng),
                map: $scope.Map,
                icon: $scope.TripPath.Stops[i].StopId === centerCoord.StopId ? "../icons/map-pin-green.png" : "../../icons/map-pin-pink.png"
            });

            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent($scope.TripPath.Stops[i].label);
                    infowindow.open($scope.Map, marker);
                };
            })(marker, i));
        }

        var tripPath = new google.maps.Polyline({
            path: $scope.TripPath.Path,
            geodesic: true,
            strokeColor: '#424242',
            strokeOpacity: 1.0,
            strokeWeight: 2
        });

        tripPath.setMap($scope.Map);
    };

    $scope.GetRoles = function () {
        SharedServices.Post('/User/GetRoles', { Token: $scope.Token }).then(function (myResponse) {
            if (myResponse.IsSuccessful)
                $scope.Roles = myResponse.response.data;
            else
                console.log("An error occurred while getting Roles.");
        });
    };

    $scope.GetTripPath = function (tripId) {
        SharedServices.Post('/TripBackend/GetTripPath', { TripId: tripId, Token: $scope.Token }).then(function (myResponse) {
            if (myResponse.IsSuccessful) {
                $scope.TripPath = myResponse.response.data;
                $scope.TripPath.Stops.push($scope.TripPath.Destination);

                var centerCoord = $scope.Passenger.UserStops.find(function (element) {
                    return element.TripId === tripId && element.StopId !== 0;
                });

                $scope.initMap(centerCoord);
            }
            else
                console.log("An error occurred while getting Trip's path.");
        });
    };

    $scope.GetPassengerDetails = function (passengerId) {
        SharedServices.Post('/Passenger/GetPassengerDetails', { UserId: passengerId, Token: $scope.Token }).then(function (myResponse) {
            if (myResponse.IsSuccessful) {
                $scope.Passenger = myResponse.response.data;
                if ($scope.Passenger.Trips != null && $scope.Passenger.Trips.length != 0)
                    $scope.GetTripPath($scope.Passenger.Trips[0].TripId);
            }
            else
                console.log("An error occurred while getting Passenger details.");
        });
    };

    $scope.GetNotifications = function (userId) {
        SharedServices.Post('/Notification/GetPassengerNotifications', { UserId: userId, Token: $scope.Token }).then(function (myResponse) {
            if (myResponse.IsSuccessful) {
                $scope.Notifications = myResponse.response.data;
                for (i = 0; i < $scope.Notifications.length; i++)
                    $scope.Notifications[i].Datetime = new Date($scope.Notifications[i].Datetime * 1000);
            }
            else
                console.log("An error occurred while getting Notifications.");
        });
    };

    $scope.GetAllTrips = function () {
        SharedServices.Post('/Trip/GetTrips', { Token: $scope.Token }).then(function (myResponse) {
            if (myResponse.IsSuccessful) {
                $scope.AllTrips = myResponse.response.data;

                var data = {
                    Type: 'Trip',
                    SelectedOptions: Array.from($scope.Passenger.Trips).map(function (trip) {
                        return {
                            Id: trip.TripId,
                            Label: trip.TripName
                        };
                    }),
                    AllOptions: $scope.AllTrips.map(function (trip) {
                        return {
                            Id: trip.TripId,
                            Label: trip.TripName
                        };
                    })
                };
                $scope.SelectOptions(data);
            }
            else
                console.log("An error occurred while getting Trips.");
        });
    };

    $scope.SelectOptions = function (data) {
        SharedServices.OpenModal('../Views/Shared/Options.html', 'OptionsCtrl', 'md', data).result.then(function (OptionResult) {
            $scope.Passenger.Trips = [];
            for (i = 0; i < OptionResult.SelectedOptions.length; i++)
                $scope.Passenger.Trips[i] = {
                    TripId: OptionResult.SelectedOptions[i].Id,
                    TripName: OptionResult.SelectedOptions[i].Label
                };
        });
    };

    $scope.SavePassenger = function (IsValid) {
        if (IsValid) {
            var data = $scope.Passenger;
            data.UserId = data.PassengerId;
            data.Options = $scope.Passenger.Trips != null ? Array.isArray($scope.Passenger.Trips) ? Array.from($scope.Passenger.Trips).map(function (trip) {
                return trip.TripId;
            }) : Object.values($scope.Passenger.Trips).map(function (trip) { return trip.TripId; }) : [];
            data.Token = $scope.Token;
            for (var i = 0; i < $scope.Roles.length; i++) {
                if ($scope.Roles[i].RoleName === "Passenger") {
                    data.RoleId = $scope.Roles[i].RoleId;
                    data.RoleName = $scope.Roles[i].RoleName;
                    break;
                }
            }

            SharedServices.Post('/User/SaveUser', data).then(function (myResponse) {
                if (myResponse.IsSuccessful) {
                    SharedServices.Alert('Passenger has been edited successfully.');
                    $scope.GetPassengerDetails($scope.Passenger.PassengerId);
                }
                else
                    SharedServices.Alert('Passenger could not be saved.');
            });
        }
    };
}]);