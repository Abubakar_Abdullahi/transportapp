﻿// RECOVERY FIXED

using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Web.Mvc;
using TransportApp.Hubs;
using TransportApp.Models;
using TransportApp.Security;
using static TransportApp.Controllers.Tools;

namespace TransportApp.Controllers
{
    public class MonitorController : Controller
    {
        private TransportEntities db = new TransportEntities();

        public ActionResult Index()
        {
            var RequestToken = Request.Cookies.Get("Token");
            if (RequestToken != null && JwtManager.ValidateToken(RequestToken.Value, ManagerRole) != 0)
            {
                ViewBag.Name = Request.Cookies.Get("Name").Value;
                return View();
            }
            else
            {
                ViewBag.RedirectLocation = "/Monitor";
                return RedirectToAction("Login", "User");
            }
        }

        public JsonResult MonitorLocation(string serverInput)
        {
            JsonResult result = new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            LocationData location;

            try
            {
                location = JsonConvert.DeserializeObject<LocationData>(serverInput);
            }
            catch
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Arguement passed could not be processed.";
                result.Data = "Arguement passed could not be processed.";
                return result;
            }

            if (JwtManager.ValidateToken(location.Token, AllRoles) == 0)
            {
                Response.StatusCode = 401;
                result.Data = "User does not have access to the resource.";
                return result;
            }

            try
            {
                IHubContext hubContext = GlobalHost.ConnectionManager.GetHubContext<MonitorHub>();
                hubContext.Clients.All.MonitorLocation(location);

                db.BusCoordinates.Add(new BusCoordinate
                {
                    TripId = location.TripId,
                    Latitude = location.Latitude,
                    Longitude = location.Longitude,
                    Passengers = db.Passengers.Where(p => location.PassengersId.Contains(p.PassengerId)).ToList(),
                    Timestamp = location.Timestamp
                });

                db.SaveChanges();
                result.Data = "Location received and processed successfully.";
            }
            catch
            {
                Response.StatusCode = 500;
                Response.StatusDescription = "Coordinates could not be saved.";
                result.Data = "Coordinates could not be saved.";
            }

            return result;
        }
    }
}
