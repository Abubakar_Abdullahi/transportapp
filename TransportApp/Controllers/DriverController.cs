﻿// RECOVERY FIXED

using Newtonsoft.Json;
using System;
using System.Linq;
using System.Web.Mvc;
using TransportApp.Models;
using TransportApp.Security;
using static TransportApp.Controllers.Tools;

namespace TransportApp.Controllers
{
    public class DriverController : Controller
    {
        private TransportEntities db = new TransportEntities();

        public ActionResult Index()
        {
            var RequestToken = Request.Cookies.Get("Token");
            if (RequestToken != null && JwtManager.ValidateToken(RequestToken.Value, ManagerRole) != 0)
            {
                ViewBag.Name = Request.Cookies.Get("Name").Value;
                return View();
            }
            else
            {
                ViewBag.RedirectLocation = "/Driver";
                return RedirectToAction("Login", "User");
            }
        }

        public ActionResult Details()
        {
            var RequestToken = Request.Cookies.Get("Token");
            if (RequestToken != null && JwtManager.ValidateToken(RequestToken.Value, ManagerRole) != 0)
            {
                ViewBag.Name = Request.Cookies.Get("Name").Value;
                return View();
            }
            else
            {
                ViewBag.RedirectLocation = "/Driver";
                return RedirectToAction("Login", "User");
            }
        }

        public JsonResult GetDriversToday(string serverInput)
        {
            JsonResult result = new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            string token;

            try
            {
                token = JsonConvert.DeserializeObject<UserData>(serverInput).Token;
            }
            catch
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "The argument passed could not be processed.";
                result.Data = "The argument passed could not be processed.";
                return result;
            }

            if (JwtManager.ValidateToken(token, Tools.AllRoles) == 0)
            {
                Response.StatusCode = 401;
                result.Data = "User does not have access to the resource.";
                return result;
            }

            try
            {
                DateTime today = DateTime.Today.Date;
                result.Data = db.Drivers.Select(driver => new
                {
                    driver.DriverId,
                    driver.User.RoleId,
                    driver.User.Role.RoleName,
                    Name = driver.User.Name + " " + driver.User.Surname,
                    driver.User.PhoneNo,
                    CollectCount = driver.Trips.Where(trip => trip.Type == "Collect").Count(),
                    DropCount = driver.Trips.Where(trip => trip.Type == "Drop").Count(),
                    Status = driver.Trips.Any(trip => trip.Status) ? "On a trip" : "Free",
                    TripsPerformed = driver.Trips.SelectMany(trip => trip.TripReports).Count(tr => tr.ActualEndTime.Year == today.Year && tr.ActualEndTime.Month == today.Month && tr.ActualEndTime.Day == today.Day)
                }).ToList();
            }
            catch
            {
                Response.StatusCode = 500;
                Response.StatusDescription = "Drivers data could not be retrieved.";
                result.Data = "Drivers data could not be retrieved.";
            }

            return result;
        }

        public JsonResult GetDrivers(string serverInput)
        {
            JsonResult result = new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            string token;

            try
            {
                token = JsonConvert.DeserializeObject<UserData>(serverInput).Token;
            }
            catch
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "The argument passed could not be processed.";
                result.Data = "The argument passed could not be processed.";
                return result;
            }

            if (JwtManager.ValidateToken(token, ManagerRole) == 0)
            {
                Response.StatusCode = 401;
                result.Data = "User does not have access to the resource.";
                return result;
            }

            try
            {
                result.Data = db.Drivers.Select(driver => new
                {
                    driver.DriverId,
                    Name = driver.User.Name + " " + driver.User.Surname,
                    driver.User.Gender,
                    driver.User.PhoneNo,
                    driver.User.Email,
                    CollectCount = driver.Trips.Where(trip => trip.Type == "Collect").Count(),
                    DropCount = driver.Trips.Where(trip => trip.Type == "Drop").Count(),
                    Status = driver.Trips.Any(trip => trip.Status) ? "On a trip" : "Free"
                }).ToList();
            }
            catch
            {
                Response.StatusCode = 500;
                Response.StatusDescription = "Drivers data could not be retrieved.";
                result.Data = "Drivers data could not be retrieved.";
            }

            return result;
        }

        public JsonResult GetDriverDetails(string serverInput)
        {
            JsonResult result = new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            UserData userData;

            try
            {
                userData = JsonConvert.DeserializeObject<UserData>(serverInput);
            }
            catch
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "The argument passed could not be processed.";
                result.Data = "The argument passed could not be processed.";
                return result;
            }

            if (JwtManager.ValidateToken(userData.Token, ManagerDriver) == 0)
            {
                Response.StatusCode = 401;
                result.Data = "User does not have access to the resource.";
                return result;
            }

            try
            {
                Driver driver = db.Drivers.Find(userData.UserId);
                DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0);

                UserDetails userDetails = new UserDetails
                {
                    DriverId = driver.DriverId,
                    Name = driver.User.Name,
                    Surname = driver.User.Surname,
                    Gender = driver.User.Gender,
                    PhoneNo = driver.User.PhoneNo,
                    Email = driver.User.Email,
                    Status = driver.Trips.Any(trip => trip.Status) ? "On a trip" : "Free",
                    CollectCount = driver.Trips.Where(trip => trip.Type == "Collect").Count(),
                    DropCount = driver.Trips.Where(trip => trip.Type == "Drop").Count(),
                    Trips = driver.Trips.Select(trip => new TripInfo
                    {
                        TripId = trip.TripId,
                        TripName = trip.Type + "Trip_" + trip.DestinationName,
                        PassengerCount = trip.TripPassengers.Count,
                        StartTimestamp = trip.StartTime,
                        EndTimestamp = trip.EndTime
                    }).ToList()
                };
                foreach (TripInfo trip in userDetails.Trips)
                {
                    trip.StartTime = dateTime.AddSeconds(trip.StartTimestamp).ToShortTimeString();
                    trip.EndTime = dateTime.AddSeconds(trip.EndTimestamp).ToShortTimeString();
                }

                result.Data = userDetails;
            }
            catch
            {
                Response.StatusCode = 500;
                Response.StatusDescription = "Drivers data could not be retrieved.";
                result.Data = "Drivers data could not be retrieved.";
            }

            return result;
        }
    }
}
