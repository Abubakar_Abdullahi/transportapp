﻿
TransportApp.service('SharedServices', ['$http', '$uibModal', function ($http, $uibModal) {
    
    this.Alert = function (notice) {
        return $uibModal.open({
            animation: true,
            templateUrl: "../Views/Shared/Notification.html",
            controller: "NotificationCtrl",
            size: 'md',
            resolve: {
                passedData: { notice: notice }
            }
        });
    };

    this.OpenModal = function (templateUrl, templateCtrl, size, data, flag, serverUrl, renderedFunc, dataList1) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: templateUrl,
            controller: templateCtrl,
            size: size,
            backdrop: 'static',
            resolve: {
                passData: {
                    data: data,
                    flag: flag,
                    url: serverUrl,
                    dataList1: dataList1
                }
            }
        });

        modalInstance.rendered.then(function () {
            if (renderedFunc)
                renderedFunc();
        });

        return modalInstance;
    };

    this.Post = function (url, data) {
        return $http.post(url, { serverInput: JSON.stringify(data) }).then(
            function successCallBack(response) {
                return { IsSuccessful: true, response: response };
            },
            function errorCallBack(response) {
                return { IsSuccessful: false, response: response };
            }
        );
    };

    this.GetCookie = function (cookieName) {
        var re = new RegExp(cookieName + "=([^;]+)");
        var value = re.exec(document.cookie);
        return value != null ? unescape(value[1]) : null;
    };

    this.UploadFile = function (url, data) {
        return $http.post(url, { serverInput: JSON.stringify(data) }).then(
            function successCallBack(response) {
                return { IsSuccessful: true, response: response };
            },
            function errorCallBack(response) {
                return { IsSuccessful: false, response: response };
            }
        );
    };

    this.DirtifyForm = function () {
        var inputElements = document.getElementsByTagName("input");
        var selectElements = document.getElementsByTagName("select");
        for (i = 0; i < inputElements.length; i++)
            inputElements[i].className += " ng-dirty";
        for (i = 0; i < selectElements.length; i++)
            selectElements[i].className += " ng-dirty";
    };

    this.LocalToUTC = function (localTime) {
        var localTimeMilliSec = localTime.getTime(); // converts the local time to milliseconds from 01/01/1970
        var localTimeOffset = localTime.getTimezoneOffset() * 60000; // 60000 is multiplied to convert minutes to milliseconds
        var utcTime = localTimeMilliSec + localTimeOffset;

        return new Date(utcTime);
    };

    this.UTCToLocal = function (utcTime) {
        var localOffset = new Date().getTimezoneOffset() * 60000; // 60000 is multiplied to convert minutes to milliseconds
        var utcTimeMilliSec = utcTime.getTime(); // converts the UTC time to milliseconds from 01/01/1970
        var localTime = utcTimeMilliSec + localOffset;

        return new Date(localTime);
    };
}]);