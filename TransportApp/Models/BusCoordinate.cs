//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TransportApp.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class BusCoordinate
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BusCoordinate()
        {
            this.Passengers = new HashSet<Passenger>();
        }
    
        public int BusCoordinateId { get; set; }
        public int TripId { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int Timestamp { get; set; }
    
        public virtual Trip Trip { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Passenger> Passengers { get; set; }
    }
}
