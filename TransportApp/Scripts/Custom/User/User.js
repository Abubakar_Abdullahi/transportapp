﻿
TransportApp.controller('UserCtrl', ['$scope', 'SharedServices', function ($scope, SharedServices) {

    $scope.User;
    $scope.LoginCredentials;
    $scope.Token;
    $scope.RedirectLocation;
    $scope.ShowError = false;

    $scope.init = function () {
        $scope.Token = Cookies.get("Token");
    };

    $scope.Login = function () {
        SharedServices.Post('/User/Login', { Username: $scope.LoginCredentials.Username, Password: $scope.LoginCredentials.Password }).then(function (myResponse) {
            if (myResponse.IsSuccessful) {
                $scope.User = myResponse.response.data;

                var today = new Date();
                var cookieExpiryDate = new Date(today.setMonth(today.getMonth() + 3)).toUTCString();
                document.cookie = "Token=" + $scope.User.Token + ";expires=" + cookieExpiryDate + ";path=/";
                document.cookie = "Name=" + $scope.User.Name + ";expires=" + cookieExpiryDate + ";path=/";
                if (typeof $scope.RedirectLocation != "undefined")
                    window.open($scope.RedirectLocation, '_self');
                else
                    window.location = "/Monitor";
            }
            else {
                $scope.ShowError = true;
                console.log("An error occurred while Logging in.");
            }   
        });
    };

    $scope.Logout = function () {
        Cookies.remove("Token");
        Cookies.remove("Name");
        window.location = "/User/Login";
    };

}]);