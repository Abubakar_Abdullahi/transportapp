﻿
TransportApp.controller('DriverDetailsCtrl', ['$scope', 'SharedServices', function ($scope, SharedServices) {

    $scope.PassData;
    $scope.Driver;
    $scope.Notifications;
    $scope.AllTrips;
    $scope.Roles;
    $scope.Token;

    $scope.init = function () {
        $scope.Token = Cookies.get("Token");
        $scope.PassData = window.passData || JSON.parse(window.sessionStorage.passData);
        $scope.GetDriverDetails($scope.PassData.Driver.DriverId);
        $scope.GetNotifications($scope.PassData.Driver.DriverId);
        $scope.GetRoles();
    };

    $scope.GetRoles = function () {
        SharedServices.Post('/User/GetRoles', { Token: $scope.Token }).then(function (myResponse) {
            if (myResponse.IsSuccessful)
                $scope.Roles = myResponse.response.data;
            else
                console.log("An error occurred while getting Roles.");
        });
    };

    $scope.GetDriverDetails = function (driverId) {
        SharedServices.Post('/Driver/GetDriverDetails', { UserId: driverId, Token: $scope.Token }).then(function (myResponse) {
            if (myResponse.IsSuccessful) {
                $scope.Driver = myResponse.response.data;
            }
            else
                console.log("An error occurred while getting Driver details.");
        });
    };

    $scope.GetNotifications = function (userId) {
        SharedServices.Post('/Notification/GetDriverNotifications', { UserId: userId, Token: $scope.Token }).then(function (myResponse) {
            if (myResponse.IsSuccessful) {
                $scope.Notifications = myResponse.response.data;
                for (i = 0; i < $scope.Notifications.length; i++)
                    $scope.Notifications[i].Datetime = new Date($scope.Notifications[i].Datetime * 1000);
            }
            else
                console.log("An error occurred while getting Notifications.");
        });
    };

    $scope.GetAllTrips = function () {
        SharedServices.Post('/Trip/GetTrips', { Token: $scope.Token }).then(function (myResponse) {
            if (myResponse.IsSuccessful) {
                $scope.AllTrips = myResponse.response.data;

                var data = {
                    Type: 'Driver',
                    SelectedOptions: Array.from($scope.Driver.Trips).map(function (trip) {
                        return {
                            Id: trip.TripId,
                            Label: trip.TripName
                        };
                    }),
                    AllOptions: $scope.AllTrips.map(function (trip) {
                        return {
                            Id: trip.TripId,
                            Label: trip.TripName
                        };
                    })
                };
                $scope.SelectOptions(data);
            }
            else
                console.log("An error occurred while getting Trips.");
        });
    };

    $scope.SelectOptions = function (data) {
        SharedServices.OpenModal('../Views/Shared/Options.html', 'OptionsCtrl', 'md', data).result.then(function (OptionResult) {
            $scope.Driver.Trips = [];
            for (i = 0; i < OptionResult.SelectedOptions.length; i++)
                $scope.Driver.Trips[i] = {
                    TripId: OptionResult.SelectedOptions[i].Id,
                    TripName: OptionResult.SelectedOptions[i].Label
                };
        });
    };

    $scope.SaveDriver = function (IsValid) {
        if (IsValid) {
            var data = $scope.Driver;
            data.UserId = data.DriverId;
            data.Options = $scope.Driver.Trips != null ? Array.isArray($scope.Driver.Trips) ? Array.from($scope.Driver.Trips).map(function (trip) {
                return trip.TripId;
            }) : Object.values($scope.Driver.Trips).map(function (trip) { return trip.TripId; }) : [];
            data.Token = $scope.Token;
            for (var i = 0; i < $scope.Roles.length; i++) {
                if ($scope.Roles[i].RoleName === "Driver") {
                    data.RoleId = $scope.Roles[i].RoleId;
                    data.RoleName = $scope.Roles[i].RoleName;
                    break;
                }
            }

            SharedServices.Post('/User/SaveUser', data).then(function (myResponse) {
                if (myResponse.IsSuccessful) {
                    SharedServices.Alert('Driver has been edited successfully.');
                    $scope.GetDriverDetails($scope.Driver.DriverId);
                }
                else
                    SharedServices.Alert('Driver could not be saved.');
            });
        }
    };
}]);