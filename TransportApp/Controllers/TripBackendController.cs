﻿// RECOVERY FIXED

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TransportApp.Models;
using TransportApp.Security;
using static TransportApp.Controllers.Tools;

namespace TransportApp.Controllers
{
    public class TripBackendController : Controller
    {
        private TransportEntities db = new TransportEntities();

        public JsonResult GetTripOverview(string serverInput)
        {
            JsonResult result = new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            TripData tripIdAndToken;

            try
            {
                tripIdAndToken = JsonConvert.DeserializeObject<TripData>(serverInput);
            }
            catch
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "The argument passed could not be processed.";
                result.Data = "The argument passed could not be processed.";
                return result;
            }

            if (JwtManager.ValidateToken(tripIdAndToken.Token, ManagerRole) == 0)
            {
                Response.StatusCode = 401;
                result.Data = "User does not have access to the resource.";
                return result;
            }

            try
            {
                Trip trip = db.Trips.Find(tripIdAndToken.TripId);

                result.Data = new
                {
                    trip.TripId,
                    trip.Type,
                    TripName = (trip.Type + "Trip_" + trip.DestinationName),
                    trip.DestinationName,
                    trip.StartTime,
                    trip.EndTime,
                    trip.Status
                };
            }
            catch
            {
                Response.StatusCode = 500;
                Response.StatusDescription = "Trip's overview data could not be retrieved.";
                result.Data = "Trip's overview data could not be retrieved";
            }

            return result;
        }

        public JsonResult GetOngoingTrips(string serverInput)
        {
            JsonResult result = new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            int userId;

            try
            {
                userId = JsonConvert.DeserializeObject<User>(serverInput).UserId;
            }
            catch
            {
                Response.StatusCode = 400;
                result.Data = "The argument passed could not be processed.";
                return result;
            }

            try
            {
                DateTime today = DateTime.Today;
                var trips = db.Trips.Where(trip => trip.Status).Select(trip => new
                {
                    trip.TripId,
                    trip.Type,
                    TripName = trip.Type + "_" + trip.DestinationName,
                    Status = "Active",
                    trip.StartTime,
                    trip.EndTime,
                    //ActualStartTime = trip.StartTime, //db.TripReports.Last(tr => tr.TripId == trip.TripId && tr.ActualStartTime.Year == today.Year && tr.ActualStartTime.Month == today.Month && tr.ActualStartTime.Day == today.Day).st,
                    PassengerCount = trip.TripPassengers.Count()
                }).ToList();

                result.Data = JsonConvert.SerializeObject(trips);
            }
            catch
            {
                Response.StatusCode = 500;
                Response.StatusDescription = "Ongoing Trips could not be retrieved.";
                result.Data = "Ongoing Trips could not be retrieved";
            }
            return result;
        }

        public JsonResult GetTripPath(string serverInput)
        {
            JsonResult result = new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            StopData stopData;

            try
            {
                stopData = JsonConvert.DeserializeObject<StopData>(serverInput);
            }
            catch
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "The argument passed could not be processed.";
                result.Data = "The argument passed could not be processed.";
                return result;
            }

            if (JwtManager.ValidateToken(stopData.Token, ManagerRole) == 0)
            {
                Response.StatusCode = 401;
                result.Data = "User does not have access to the resource.";
                return result;
            }

            try
            {
                Trip trip = db.Trips.Find(stopData.TripId);
                int TimestampDateComponent, CompareTimestampDateComponent = 0;
                List<TripPath> tripPathList = new List<TripPath>();
                for (int i = trip.BusCoordinates.Count - 1; i >= 0; --i)
                {
                    TimestampDateComponent = (int)Math.Floor(trip.BusCoordinates.ElementAt(i).Timestamp / 84600.0) * 84600;
                    if (i == trip.BusCoordinates.Count - 1) // set date component to compare on first itertion
                        CompareTimestampDateComponent = TimestampDateComponent;
                    else if (TimestampDateComponent != CompareTimestampDateComponent) // 
                        break;
                    tripPathList.Add(new TripPath()
                    {
                        lat = trip.BusCoordinates.ElementAt(i).Latitude,
                        lng = trip.BusCoordinates.ElementAt(i).Longitude
                    });
                }

                result.Data = new
                {
                    trip.TripId,
                    TripName = (trip.Type + "Trip_" + trip.DestinationName),
                    Destination = (trip.DestinationId.HasValue ? new
                    {
                        StopId = trip.DestinationId,
                        lat = trip.Stop.Latitude,
                        lng = trip.Stop.Longitude,
                        label = trip.DestinationName
                    } : null),
                    Stops = (trip.TripPassengers.Count != 0 ? trip.TripPassengers.OrderBy(tp => tp.SortingOrder).Select(tp => tp.Stop).Distinct().Where(stop => stop != null).Select(stop => new
                    {
                        stop.StopId,
                        label = string.Join(", ", stop.TripPassengers.Select(tp => tp.Passenger.User.Name + " " + tp.Passenger.User.Surname)),
                        lat = stop.Latitude,
                        lng = stop.Longitude
                    }) : null),
                    Path = tripPathList
                };
            }
            catch
            {
                Response.StatusCode = 500;
                Response.StatusDescription = "Trip's path could not be retrieved.";
                result.Data = "Trip's path could not be retrieved";
            }

            return result;
        }

        public JsonResult GetPassengers(string serverInput) 
        {
            JsonResult result = new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            TripData tripData;

            try
            {
                tripData = JsonConvert.DeserializeObject<TripData>(serverInput);
            }
            catch
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "The argument passed could not be processed.";
                result.Data = "The argument passed could not be processed.";
                return result;
            }

            if (JwtManager.ValidateToken(tripData.Token, ManagerDriver) == 0)
            {
                Response.StatusCode = 401;
                result.Data = "User does not have access to the resource.";
                return result;
            }

            try
            {
                Trip trip = db.Trips.Find(tripData.TripId);
                if (trip != null)
                {
                    result.Data = trip.TripPassengers.Select(tp => new PassengerData
                    {
                        UserId = tp.PassengerId,
                        Name = tp.Passenger.User.Name + " " + tp.Passenger.User.Surname,
                        RoleName = "Passenger",
                        Stop = tp.Stop != null ? new BackendStopData
                        {
                            StopId = tp.StopId,
                            Latitude = tp.Stop.Latitude,
                            Longitude = tp.Stop.Longitude
                        } : new BackendStopData(),
                        AssignerId = tp.AssignerId ?? 0, // same as tp.AssignerId.HasValue ? tp.AssignerId.Value : 0
                        AssignerName = tp.AssignerId.HasValue ? tp.User.Name + " " + tp.User.Surname : "Stop not yet assinged"
                    }).ToList();
                }
                else
                {
                    Response.StatusCode = 404;
                    Response.StatusDescription = "Trip with the provided TripId doesn't exist.";
                    result.Data = "Trip with the provided TripId doesn't exist.";
                }
            }
            catch
            {
                Response.StatusCode = 500;
                Response.StatusDescription = "Passengers could not be retrieved.";
                result.Data = "Passengers could not be retrieved.";
            }

            return result;
        }

        public JsonResult SaveTrip(string serverInput)
        {
            JsonResult result = new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            TripData tripData;
            Trip trip;

            try
            {
                tripData = JsonConvert.DeserializeObject<TripData>(serverInput);
            }
            catch
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "The argument passed could not be processed.";
                result.Data = "The argument passed could not be processed.";
                return result;
            }

            if (JwtManager.ValidateToken(tripData.Token, ManagerDriver) == 0)
            {
                Response.StatusCode = 401;
                result.Data = "User does not have access to the resource.";
                return result;
            }

            if (tripData.TripId == 0)
            {
                try
                {
                    trip = new Trip
                    {   
                        Type = tripData.Type,
                        StartTime = (int)((DateTimeOffset)tripData.StartTime).ToUnixTimeSeconds(),
                        EndTime = (int)((DateTimeOffset)tripData.EndTime).ToUnixTimeSeconds(),
                        DestinationName = tripData.Destination.DestinationName,
                        Drivers = new List<Driver> { db.Drivers.Find(tripData.DriverId) },
                    };

                    if (tripData.Options != null && tripData.Options.Count() > 0) // tripdata.Options is Passengers
                    {
                        foreach (int option in tripData.Options)
                        {
                            trip.TripPassengers.Add(new TripPassenger
                            {
                                PassengerId = option,
                                TripId = tripData.TripId,
                                Coming = true,
                                SortingOrder = 0
                            });
                        }
                    }

                    db.Trips.Add(trip);
                    db.SaveChanges();
                    result.Data = "Trip was added successfully.";
                }
                catch
                {
                    Response.StatusCode = 500;
                    Response.StatusDescription = "Trip could not be saved.";
                    result.Data = "Trip could not be saved.";
                }
            }
            else
            {
                try
                {
                    trip = db.Trips.Find(tripData.TripId);

                    trip.Type = tripData.Type;
                    trip.StartTime = (int)((DateTimeOffset)tripData.StartTime).ToUnixTimeSeconds();
                    trip.EndTime = (int)((DateTimeOffset)tripData.EndTime).ToUnixTimeSeconds();
                    trip.DestinationName = tripData.DestinationName;
                    trip.Stop = tripData.Destination ?? null;

                    trip.Drivers.Clear();
                    trip.Drivers = db.Drivers.Where(driver => tripData.DriversId.Contains(driver.DriverId)).ToList();

                    int[] toBeRemovedPassengerIds = trip.TripPassengers.Select(tp => tp.PassengerId).Except(tripData.PassengersId).ToArray();
                    int[] newPassengerIds = tripData.PassengersId.Except(trip.TripPassengers.Select(tp => tp.PassengerId)).ToArray();

                    foreach (int passengerId in toBeRemovedPassengerIds)
                        trip.TripPassengers.Remove(trip.TripPassengers.Single(tp => tp.PassengerId == passengerId));

                    foreach (int passengerId in newPassengerIds)
                    {
                        trip.TripPassengers.Add(new TripPassenger
                        {
                            PassengerId = passengerId,
                            SortingOrder = 0,
                            Coming = true
                        });
                    }

                    db.SaveChanges();
                    result.Data = "Trip was edited successfully.";
                }
                catch
                {
                    Response.StatusCode = 500;
                    Response.StatusDescription = "Trip could not be edited.";
                    result.Data = "Trip could not be edited.";
                }
            }

            return result;
        }

        public JsonResult DeleteTrip(string serverInput)
        {
            JsonResult result = new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            TripData tripData;

            try
            {
                tripData = JsonConvert.DeserializeObject<TripData>(serverInput);
            }
            catch
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "The arguments passed could not be processed.";
                result.Data = "The arguments passed could not be processed.";
                return result;
            }
            
            try
            {
                Trip trip = db.Trips.Find(tripData.TripId);
                if (trip == null)
                {
                    Response.StatusCode = 404;
                    Response.StatusDescription = "Trip with the provided TripId does not exist.";
                    result.Data = "Trip with the provided TripId does not exist.";
                    return result;
                }

                trip.Drivers.Clear();
                db.TripPassengers.RemoveRange(trip.TripPassengers);
                db.BusCoordinates.RemoveRange(trip.BusCoordinates);
                db.TripReports.RemoveRange(trip.TripReports);
                db.Trips.Remove(trip);

                db.SaveChanges();
                result.Data = "Trip has been deleted successfully.";
            }
            catch
            {
                Response.StatusCode = 500;
                Response.StatusDescription = "Trip could not be deleted.";
                result.Data = "Trip could not be deleted.";
            }

            return result;
        }

        private class BackendStopData
        {
            public int? StopId { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }
        }

        private class PassengerData
        {
            public int UserId { get; set; }
            public string RoleName { get; set; }
            public string Name { get; set; }
            public BackendStopData Stop { get; set; }
            public int AssignerId { get; set; }
            public string AssignerName { get; set; }
            public string Token { get; set; }
        }

        private class TripPath
        {
            public double lat { get; internal set; }
            public double lng { get; internal set; }
        }

        private class PseudoTripPassenger : TripPassenger
        {
        }
    }
}
