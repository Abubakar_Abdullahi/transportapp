﻿
TransportApp.controller('OptionsCtrl', ['$scope', '$uibModalInstance', 'passData', 'SharedServices', function ($scope, $uibModalInstance, passData, SharedServices) {

    $scope.AllOptions = [];
    $scope.SelectedOptions = [];
    $scope.Type;

    $scope.init = function () {
        $scope.AllOptions = passData.data.AllOptions;
        $scope.SelectedOptions = passData.data.SelectedOptions;
        $scope.Type = passData.data.Type;
    };

    $scope.ToggleSelection = function (option) {
        var index = $scope.GetIndexOf(option);
        
        if (index > -1) // Option is currently selected. So remove it
            $scope.SelectedOptions.splice(index, 1);
        else // Option is newly selected. So add it
            $scope.SelectedOptions.push(option);
    };

    $scope.GetIndexOf = function (option) {
        for (index = 0; index < $scope.SelectedOptions.length; index++)
            if ($scope.SelectedOptions[index].Id === option.Id)
                return index;
        return -1;
    };

    $scope.ok = function () {
        $uibModalInstance.close({
            Type: $scope.Type,
            SelectedOptions: $scope.SelectedOptions
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}]);