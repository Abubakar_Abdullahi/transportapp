﻿using System;
using System.Collections.Generic;
using TransportApp.Models;

namespace TransportApp.Controllers
{
    public class Tools
    {
        public static List<string> AllRoles = new List<string>()
        {
          "Manager",
          "Driver",
          "Passenger"
        };
        public static List<string> ManagerDriver = new List<string>()
        {
            "Manager",
            "Driver"
        };
        public static List<string> ManagerPassenger = new List<string>()
        {
            "Manager",
            "Passenger"
        };
        public static List<string> DriverPassenger = new List<string>()
        {
          "Driver",
          "Passenger"
        };
        public static List<string> ManagerRole = new List<string>()
        {
            "Manager"
        };
        public static List<string> DriverRole = new List<string>()
        {
          "Driver"
        };
        public static List<string> PassengerRole = new List<string>()
        {
          "Passenger"
        };
        public const string OneSignalHeaderValue = "Basic ODI1OWMxZGMtNTMxOS00ZDU5LWI0MWUtM2Y3NTExNzI4NmY0";
        public const string OneSignalAppId = "670789a8-9740-46e1-8f3f-1819300d48eb";

        public class Result
        {
            public Exception Exception { get; set; }
            public string Message { get; set; }
            public object Data { get; set; }
            public string Status { get; set; }
        }

        public class TripInfo
        {
            public int TripId { get; set; }
            public string TripName { get; set; }
            public string Type { get; set; }
            public string StartTime { get; set; }
            public string EndTime { get; set; }
            public int StartTimestamp { get; set; }
            public int EndTimestamp { get; set; }
            public string Status { get; set; }
            public int StopCount { get; set; }
            public int PassengerCount { get; set; }
            public int TripCount { get; set; }
            public int TotalTripCount { get; set; }
            public string DestinationName { get; set; }
        }

        public class TripData
        {
            public int TripId { get; set; }
            public int DriverId { get; set; }
            public int[] DriversId { get; set; }
            public int[] PassengersId { get; set; }
            public int[] Options { get; set; }
            public string Type { get; set; }
            public DateTime StartTime { get; set; }
            public DateTime EndTime { get; set; }
            public Destination Destination { get; set; }
            public string Token { get; set; }
            public string DestinationName { get; set; }
        }

        public class TripDetails
        {
            public string Type { get; set; }
            public List<StopData> Stops { get; set; }
            public Dictionary<string, string> Passengers { get; set; }
            public Dictionary<string, UserData> Drivers { get; set; }
            public Dictionary<string, Attendance> AttendanceList { get; set; }
            public object Destination { get; set; }
        }

        public class Attendance
        {
            public int PassengerId { get; set; }

            public string Name { get; set; }

            public string Surname { get; set; }

            public bool? Coming { get; set; }

            public string BeaconId { get; set; }

            public bool isEngaged { get; set; }
        }

        public class Destination : Stop
        {
            public string DestinationName { get; set; }

            public int TripId { get; set; }

            public string Token { get; set; }
        }

        public class AbsenceData
        {
            public int TripId { get; set; }

            public int PassengerId { get; set; }

            public string Token { get; set; }
        }

        public class StopData
        {
            public int TripId { get; set; }
            public int StopId { get; set; }
            public int[] PassengersId { get; set; }
            public List<UserData> Passengers { get; set; }
            public int AssignerId { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }
            public int Timestamp { get; set; }
            public bool isCurrent { get; set; }
            public bool isEntered { get; set; }
            public string Token { get; set; }
        }

        public class SortingData
        {
            public int TripId { get; set; }

            public IDictionary<int, int> StopsOrder { get; set; }

            public string Token { get; set; }
        }

        public class UserData
        {
            public int UserId { get; set; }
            public string RoleName { get; set; }
            public string Name { get; set; }
            public string Surname { get; set; }
            public string PhoneNo { get; set; }
            public bool Coming { get; set; }
            public string Token { get; set; }
        }

        public class UserDetails
        {
            public int DriverId { get; set; }

            public int PassengerId { get; set; }

            public string Name { get; set; }

            public string Surname { get; set; }

            public bool Gender { get; set; }

            public string PhoneNo { get; set; }

            public string Email { get; set; }

            public string BeaconId { get; set; }

            public int CollectCount { get; set; }

            public int DropCount { get; set; }

            public List<Tools.TripInfo> Trips { get; set; }

            public string Status { get; set; }

            public IEnumerable<object> UserStops { get; set; }
        }

        public class LoginCredentials
        {
            public int UserId { get; set; }

            public string RoleName { get; set; }

            public string Name { get; set; }

            public string Username { get; set; }

            public string Password { get; set; }
        }

        public class SaveUserData
        {
            public int UserId { get; set; }
            public int RoleId { get; set; }
            public int[] Options { get; set; }
            public string RoleName { get; set; }
            public string Name { get; set; }
            public string Surname { get; set; }
            public string Username { get; set; }
            public string Password { get; set; }
            public bool Gender { get; set; }
            public string PhoneNo { get; set; }
            public string Email { get; set; }
            public string BeaconId { get; set; }
            public string Token { get; set; }
        }

        public class LocationData : BusCoordinate
        {
            public int[] PassengersId { get; set; }

            public string Token { get; set; }
        }

        public class DeviceData
        {
            public int UserId { get; set; }

            public string DeviceId { get; set; }

            public string Token { get; set; }
        }

        public class NotificationData
        {
            public int SenderId { get; set; }

            public int[] ReceiversId { get; set; }

            public string NotificationType { get; set; }

            public int Delay { get; set; }

            public DateTime Datetime { get; set; }

            public int TripId { get; set; }

            public string Token { get; set; }
        }
    }
}
