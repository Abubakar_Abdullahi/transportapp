﻿
TransportApp.controller('TripDetailsCtrl', ['$scope', 'SharedServices', function ($scope, SharedServices) {

    $scope.PassData;
    $scope.Trip;
    $scope.Drivers;
    $scope.Token;
    $scope.TripPath;
    $scope.TripOverview;
    $scope.Passengers;
    $scope.AllPassengers;
    $scope.AllDrivers;
    $scope.Map;

    $scope.init = function () {
        $scope.Token = Cookies.get("Token");
        $scope.PassData = window.passData || JSON.parse(window.sessionStorage.passData);
        $scope.GetTripDetails($scope.PassData.Trip.TripId);
        $scope.Drivers = $scope.PassData.Drivers;
        $scope.GetTripPath();
        $scope.GetPassengers();
    };

    $scope.initMap = function () {
        if ($scope.TripPath.Stops !== null) {
            $scope.Map = new google.maps.Map(document.getElementById('trip-path-map'), {
                zoom: 15,
                center: { lat: $scope.TripPath.Stops[0].lat, lng: $scope.TripPath.Stops[0].lng }
            });

            var infowindow = new google.maps.InfoWindow();
            var marker, i;

            for (i = 0; i < $scope.TripPath.Stops.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng($scope.TripPath.Stops[i].lat, $scope.TripPath.Stops[i].lng),
                    map: $scope.Map,
                    icon: "../../icons/map-pin-pink.png"
                });

                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent($scope.TripPath.Stops[i].label);
                        infowindow.open($scope.Map, marker);
                    };
                })(marker, i));
            }

            var tripPath = new google.maps.Polyline({
                path: $scope.TripPath.Stops,
                geodesic: true,
                strokeColor: '#424242',
                strokeOpacity: 1.0,
                strokeWeight: 2
            });
            tripPath.setMap($scope.Map);
        }
    };

    $scope.GetTripOverview = function () {
        SharedServices.Post('/TripBackend/GetTripOverview', { TripId: $scope.PassData.Trip.TripId, Token: $scope.Token }).then(function (myResponse) {
            if (myResponse.IsSuccessful) {
                $scope.TripOverview = myResponse.response.data;
                $scope.Trip.Type = $scope.TripOverview.Type;
                $scope.Trip.DestinationName = $scope.TripOverview.DestinationName;
                $scope.Trip.StartTime = new Date($scope.TripOverview.StartTime * 1000);
                $scope.Trip.EndTime = new Date($scope.TripOverview.EndTime * 1000);
                $scope.Trip.TripName = $scope.TripOverview.TripName;
            }
            else
                console.log("An error occurred while getting Trip's overview data.");
        });
    };

    $scope.GetTripDetails = function (tripId) {
        SharedServices.Post('/Trip/GetTripDetails', { TripId: tripId, Token: $scope.Token }).then(function (myResponse) {
            if (myResponse.IsSuccessful) {
                $scope.Trip = myResponse.response.data;
                $scope.GetTripOverview();
            }   
            else
                console.log("An error occurred while getting Trip details.");
        });
    };

    $scope.GetTripPath = function () {
        SharedServices.Post('/TripBackend/GetTripPath', { TripId: $scope.PassData.Trip.TripId, Token: $scope.Token }).then(function (myResponse) {
            if (myResponse.IsSuccessful) {
                $scope.TripPath = myResponse.response.data;
                if ($scope.TripPath.Destination !== null)
                    $scope.TripPath.Stops.push($scope.TripPath.Destination);
                $scope.initMap();
            }
            else
                console.log("An error occurred while getting Trip's path.");
        });
    };

    $scope.GetPassengers = function () {
        SharedServices.Post('/TripBackend/GetPassengers', { TripId: $scope.PassData.Trip.TripId, Token: $scope.Token }).then(function (myResponse) {
            if (myResponse.IsSuccessful) {
                $scope.Passengers = myResponse.response.data;
            }
            else
                console.log("An error occurred while getting Passengers.");
        });
    };

    $scope.GetAllPassengers = function () {
        SharedServices.Post('/Passenger/GetPassengers', { Token: $scope.Token }).then(function (myResponse) {
            if (myResponse.IsSuccessful) {
                $scope.AllPassengers = myResponse.response.data;

                var passengers = $scope.Trip.Passengers != null ? Array.isArray($scope.Trip.Passengers) ? Array.from($scope.Trip.Passengers) : Object.keys($scope.Trip.Passengers) : [];
                var data = {
                    Type: 'Passenger',
                    SelectedOptions: passengers.map(function (key) {
                        return {
                            Id: parseInt(key),
                            Label: $scope.Trip.Passengers[key]
                        };
                    }),
                    AllOptions: $scope.AllPassengers.map(function (passenger) {
                        return {
                            Id: passenger.PassengerId,
                            Label: passenger.Name
                        };
                    })
                };
                $scope.SelectOptions(data);
            }
            else
                console.log("An error occurred while getting Passengers.");
        });
    };

    $scope.GetAllDrivers = function () {
        SharedServices.Post('/Driver/GetDrivers', { Token: $scope.Token }).then(function (myResponse) {
            if (myResponse.IsSuccessful) {
                $scope.AllDrivers = myResponse.response.data;

                // $scope.Trip.Drivers is initially a dictionary
                var drivers = Array.isArray($scope.Trip.Drivers) ? Array.from($scope.Trip.Drivers) : Object.values($scope.Trip.Drivers);
                var data = {
                    Type: 'Driver',
                    SelectedOptions: drivers.map(function (driver) {
                        return {
                            Id: driver.UserId,
                            Label: driver.Surname ? driver.Name + " " + driver.Surname : driver.Name
                        };
                    }),
                    AllOptions: $scope.AllDrivers.map(function (driver) {
                        return {
                            Id: driver.DriverId,
                            Label: driver.Name
                        };
                    })
                };
                $scope.SelectOptions(data);
            }
            else
                console.log("An error occurred while getting Passengers.");
        });
    };

    $scope.SelectOptions = function (data) {
        SharedServices.OpenModal('../Views/Shared/Options.html', 'OptionsCtrl', 'md', data).result.then(function (OptionsResult) {
            if (OptionsResult.Type === 'Passenger') {
                $scope.Trip.Passengers = [];
                for (i = 0; i < OptionsResult.SelectedOptions.length; i++)
                    $scope.Trip.Passengers[i] = {
                        UserId: OptionsResult.SelectedOptions[i].Id,
                        Name: OptionsResult.SelectedOptions[i].Label
                    };
            }
            else if (OptionsResult.Type === 'Driver') {
                $scope.Trip.Drivers = [];
                for (i = 0; i < OptionsResult.SelectedOptions.length; i++)
                    $scope.Trip.Drivers[i] = {
                        UserId: OptionsResult.SelectedOptions[i].Id,
                        Name: OptionsResult.SelectedOptions[i].Label
                    };
            }
        });
    };

    $scope.SetStop = function (stop) {
        $scope.Map.setZoom(13);
        $scope.Map.setCenter(new google.maps.LatLng(stop.lat, stop.lng));
    };

    $scope.SaveTrip = function (IsValid) {
        if (IsValid) {
            var data = {
                TripId: $scope.PassData.Trip.TripId,
                Type: $scope.PassData.Trip.Type,
                StartTime: $scope.Trip.StartTime,
                EndTime: $scope.Trip.EndTime,
                DestinationName: $scope.Trip.DestinationName,
                Destination: $scope.Trip.Destination,
                DriversId: $scope.Trip.Drivers !== null ? Array.isArray($scope.Trip.Drivers) ? Array.from($scope.Trip.Drivers).map(function (driver) {
                    return driver.UserId;
                }) : Object.values($scope.Trip.Drivers).map(function (driver) { return driver.UserId; }) : [],
                PassengersId: $scope.Trip.Passengers !== null ? Array.isArray($scope.Trip.Passengers) ? Array.from($scope.Trip.Passengers).map(function (passenger) {
                    return passenger.UserId;
                }) : Object.keys($scope.Trip.Passengers).map(function (key) { return parseInt(key); }) : [],
                Token: $scope.Token
            };

            SharedServices.Post('/TripBackend/SaveTrip', data).then(function (myResponse) {
                if (myResponse.IsSuccessful) {
                    SharedServices.Alert('Trip has been edited successfully.');
                    $scope.GetTripOverview();
                }   
                else
                    SharedServices.Alert('Trip could not be saved.');
            });
        }
    };
}]);