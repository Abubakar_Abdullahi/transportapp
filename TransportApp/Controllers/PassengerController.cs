﻿// RECOVERY FIXED

using Newtonsoft.Json;
using System;
using System.Linq;
using System.Web.Mvc;
using TransportApp.Models;
using TransportApp.Security;
using static TransportApp.Controllers.Tools;

namespace TransportApp.Controllers
{
    public class PassengerController : Controller
    {
        private TransportEntities db = new TransportEntities();

        public ActionResult Index()
        {
            var RequestToken = Request.Cookies.Get("Token");
            if (RequestToken != null && JwtManager.ValidateToken(RequestToken.Value, ManagerRole) != 0)
            {
                ViewBag.Name = Request.Cookies.Get("Name").Value;
                return View();
            }
            else
            {
                ViewBag.RedirectLocation = "/Passenger";
                return RedirectToAction("Login", "User");
            }
        }

        public ActionResult Details()
        {
            var RequestToken = Request.Cookies.Get("Token");
            if (RequestToken != null && JwtManager.ValidateToken(RequestToken.Value, ManagerRole) != 0)
            {
                ViewBag.Name = Request.Cookies.Get("Name").Value;
                return View();
            }
            else
            {
                ViewBag.RedirectLocation = "/Passenger";
                return RedirectToAction("Login", "User");
            }
        }

        public JsonResult GetPassengers(string serverInput)
        {
            JsonResult result = new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            string token;

            try
            {
                token = JsonConvert.DeserializeObject<UserData>(serverInput).Token;
            }
            catch
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "The argument passed could not be processed.";
                result.Data = "The argument passed could not be processed.";
                return result;
            }

            if (JwtManager.ValidateToken(token, ManagerRole) == 0)
            {
                Response.StatusCode = 401;
                result.Data = "User does not have access to the resource.";
                return result;
            }

            try
            {
                result.Data = db.Passengers.Select(passenger => new
                {
                    passenger.PassengerId,
                    Name = passenger.User.Name + " " + passenger.User.Surname,
                    passenger.User.Gender,
                    passenger.User.PhoneNo,
                    passenger.User.Email,
                    CollectCount = passenger.TripPassengers.Select(tp => tp.Trip).Where(trip => trip.Type == "Collect").Count(),
                    DropCount = passenger.TripPassengers.Select(tp => tp.Trip).Where(trip => trip.Type == "Drop").Count(),
                    Status = passenger.TripPassengers.Select(tp => tp.Trip).Any(trip => trip.Status)
                }).ToList();
            }
            catch
            {
                Response.StatusCode = 500;
                Response.StatusDescription = "Passengers data could not be retrieved.";
                result.Data = "Passengers data could not be retrieved.";
            }

            return result;
        }

        public JsonResult GetPassengerDetails(string serverInput)
        {
            JsonResult result = new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            UserData userData;

            try
            {
                userData = JsonConvert.DeserializeObject<UserData>(serverInput);
            }
            catch
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "The argument passed could not be processed.";
                result.Data = "The argument passed could not be processed.";
                return result;
            }

            if (JwtManager.ValidateToken(userData.Token, ManagerPassenger) == 0)
            {
                Response.StatusCode = 401;
                result.Data = "User does not have access to the resource.";
                return result;
            }
            try
            {
                Passenger passenger = db.Passengers.Find(userData.UserId);
                DateTime date1970 = new DateTime(1970, 1, 1, 0, 0, 0);

                UserDetails passengerDetails = new UserDetails
                {
                    PassengerId = passenger.PassengerId,
                    Name = passenger.User.Name,
                    Surname = passenger.User.Surname,
                    Gender = passenger.User.Gender,
                    PhoneNo = passenger.User.PhoneNo,
                    BeaconId = passenger.BeaconId,
                    Email = passenger.User.Email,
                    Status = passenger.TripPassengers.Any(tp => tp.Trip.Status) ? "On a trip" : "Free",
                    CollectCount = passenger.TripPassengers.Select(tp => tp.Trip).Where(trip => trip.Type == "Collect").Count(),
                    DropCount = passenger.TripPassengers.Select(tp => tp.Trip).Where(trip => trip.Type == "Drop").Count(),
                    UserStops = passenger.TripPassengers.Select(tp => new
                    {
                        tp.TripId,
                        StopId = tp.StopId ?? 0,
                        lat = tp.StopId.HasValue ? tp.Stop.Latitude : 0.0,
                        lng = tp.StopId.HasValue ? tp.Stop.Longitude : 0.0
                    }),
                    Trips = passenger.TripPassengers.Select(tp => tp.Trip).Select(trip =>
                    {
                        return new TripInfo
                        {
                            TripId = trip.TripId,
                            TripName = trip.Type + "Trip_" + trip.DestinationName,
                            TripCount = trip.TripReports.Count,
                            TotalTripCount = db.TripReports.Count(tr => tr.TripId == trip.TripId),
                            StartTimestamp = trip.StartTime,
                            EndTimestamp = trip.EndTime
                        };
                    }).ToList()
                };

                foreach (TripInfo trip in passengerDetails.Trips)
                {
                    trip.StartTime = date1970.AddSeconds(trip.StartTimestamp).ToShortTimeString();
                    trip.EndTime = date1970.AddSeconds(trip.EndTimestamp).ToShortTimeString();
                }
                result.Data = passengerDetails;
            }
            catch
            {
                Response.StatusCode = 500;
                Response.StatusDescription = "Passengers data could not be retrieved.";
                result.Data = "Passengers data could not be retrieved.";
            }

            return result;
        }
    }
}
