﻿
TransportApp.controller('DriverCtrl', ['$scope', 'SharedServices', function ($scope, SharedServices) {

    $scope.Drivers;
    $scope.Trips;
    $scope.Token;

    $scope.init = function () {
        $scope.Token = Cookies.get("Token");
        $scope.GetDrivers();
        $scope.GetNotifications();
        $scope.GetRoles();
    };

    $scope.GetRoles = function () {
        SharedServices.Post('/User/GetRoles', { Token: $scope.Token }).then(function (myResponse) {
            if (myResponse.IsSuccessful)
                $scope.Roles = myResponse.response.data;
            else
                console.log("An error occurred while getting Roles.");
        });
    };

    $scope.GetDrivers = function () {
        SharedServices.Post('/Driver/GetDrivers', { Token: $scope.Token }).then(function (myResponse) {
            if (myResponse.IsSuccessful)
                $scope.Drivers = myResponse.response.data;
            else
                console.log("An error occurred while getting Drivers.");
        });
    };

    $scope.GetNotifications = function () {
        SharedServices.Post('/Notification/GetDriverNotifications', { Token: $scope.Token }).then(function (myResponse) {
            if (myResponse.IsSuccessful) {
                $scope.Notifications = myResponse.response.data;
                for (i = 0; i < $scope.Notifications.length; i++)
                    $scope.Notifications[i].Datetime = new Date($scope.Notifications[i].Datetime*1000);
            }
            else
                console.log("An error occurred while getting Notifications.");
        });
    };

    $scope.AddDriver = function () {
        var data;
        for (var i = 0; i < $scope.Roles.length; i++) {
            if ($scope.Roles[i].RoleName === "Driver") {
                data = $scope.Roles[i];
                break;
            }   
        }

        SharedServices.OpenModal("../Views/Driver/DriverForm.html", "FormCtrl", "lg", data, true, "/User/SaveUser", null, null).result.then(function () {
            $scope.init();
        });
    };

    $scope.DriverDetails = function (driver) {
        var driverWindow = window.open('/Driver/Details', '_self');
        var data = {
            Driver: driver,
            Trips: $scope.Trips
        };
        var dataStr = JSON.stringify(data);
        driverWindow.passData = data;
        driverWindow.sessionStorage.passData = dataStr;
    };

    $scope.DeleteDriver = function (driver) {
        var data = {
            UserId: driver.DriverId,
            Token: $scope.Token
        };
        SharedServices.OpenModal("../Views/Shared/ConfirmDelete.html", "DeleteItemCtrl", "md", data, null, "/User/DeleteUser").result.then(function () {
            $scope.init();
        });
    };
}]);