﻿
TransportApp.controller('NotificationCtrl', ['$scope', '$uibModalInstance', 'passedData', function ($scope, $uibModalInstance, passedData) {

    $scope.note = passedData.notice;

    $scope.init = function () {
        setTimeout(function () {
            $uibModalInstance.close();
        }, 2000);
    };

    $scope.ok = function () {
        $uibModalInstance.close();
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}]);