﻿TransportApp.controller('MonitorCtrl', ['$scope', 'SharedServices', function ($scope, SharedServices) {

    $scope.CurrentPassengerCount;
    $scope.BusMarker;
    $scope.Trips;
    $scope.DriversToday;
    $scope.Notifications;
    $scope.TripPath;
    $scope.CurrentTripId;
    $scope.Token;
    $scope.Locations = [
        { name: "Stop1: Abubakar, Kamal", lat: 37.0344065, lng: 37.3161016 },
        { name: "Stop2: Usman", lat: 37.035732, lng: 37.319961 },
        { name: "Stop3: Hasan", lat: 37.035012, lng: 37.320691 },
        { name: "Stop4: Ibrahim", lat: 37.034127, lng: 37.320519 },
        { name: "Stop5: Jabir", lat: 37.034417, lng: 37.321613 }
    ];
    
    var SetOngoingTrips = function (trips) {
        $scope.Trips = trips;
        for (i = 0; i < $scope.Trips.length; i++) {
            if ($scope.Trips[i].ActualStartTime)
                $scope.Trips[i].ActualStartTime = new Date($scope.Trips[i].ActualStartTime);
            if ($scope.Trips[i].ActualEndTime)
                $scope.Trips[i].ActualEndTime = new Date($scope.Trips[i].ActualEndTime);
        }
    };

    $scope.init = function () {
        $scope.Token = Cookies.get("Token");
        $scope.StartSignalR();
        $scope.GetOngoingTrips();
        $scope.GetDriversToday();
        $scope.GetNotifications();

        var $tripsGroup = $('#tripLists');
        $tripsGroup.on('show.bs.collapse', '.collapse', function (event) {
            $tripsGroup.find('.collapse.in').collapse('hide'); // collapses all other trips
            $scope.GetTripPath(event.currentTarget.dataset.tripid);
            $scope.CurrentTripId = event.currentTarget.dataset.tripid;
        });
    };

    $scope.initMap = function () {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 17,
            center: { lat: $scope.TripPath.Destination.lat, lng: $scope.TripPath.Destination.lng }
        });

        $scope.BusMarker = new google.maps.Marker({
            //position: new google.maps.LatLng(37.036633, 37.319343),
            map: map,
            icon: "../../icons/bus.png"
        });

        var infowindow = new google.maps.InfoWindow();
        var marker, i;
        for (i = 0; i < $scope.TripPath.Stops.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng($scope.TripPath.Stops[i].lat, $scope.TripPath.Stops[i].lng),
                map: map,
                icon: "../../icons/map-pin-pink.png"
            });

            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent($scope.TripPath.Stops[i].label);
                    infowindow.open(map, marker);
                };
            })(marker, i));
        }

        /*var tripPath = new google.maps.Polyline({
            path: $scope.TripPath.Path,
            geodesic: true,
            strokeColor: '#424242',
            strokeOpacity: 1.0,
            strokeWeight: 2
        });
        tripPath.setMap(map);*/
    };
    
    $scope.StartSignalR = function () {
        var myHub = $.connection.monitorHub;
        $.connection.hub.start();

        myHub.client.MonitorLocation = function (coordinate) {
            if (coordinate.TripId == $scope.CurrentTripId) {
                $scope.Update(coordinate);
            }
        };

        myHub.client.RetrieveOngoingTrips = function (OngoingTripsData) {
            $scope.$apply(SetOngoingTrips(OngoingTripsData.CurrentTrips));

            if (OngoingTripsData.ChangedTrip.Started)
                toastr.success(OngoingTripsData.ChangedTrip.TripName + 'has started.');
            else
                toastr.error(OngoingTripsData.ChangedTrip.TripName + 'has ended.');
        };
    };

    $scope.Update = function (coordinate) {
        $scope.BusMarker.setPosition(new google.maps.LatLng(coordinate.Latitude, coordinate.Longitude));

        for (i = 0; i < $scope.Trips.length; i++) {
            if ($scope.Trips[i].TripId === coordinate.TripId) {
                $scope.Trips[i].CurrentPassengerCount = coordinate.PassengersId.length;
            }
        }
    };

    $scope.GetOngoingTrips = function () {
        SharedServices.Post('/TripBackend/GetOngoingTrips', { UserId: 0, Token: $scope.Token }).then(function (myResponse) {
            if (myResponse.IsSuccessful) {
                SetOngoingTrips(JSON.parse(myResponse.response.data));
                $scope.GetTripPath($scope.Trips[0].TripId);
                $scope.CurrentTripId = $scope.Trips[0].TripId;
            }
            else
                console.log("An error occurred while getting Trips.");
        });
    };

    $scope.GetDriversToday = function () {
        SharedServices.Post('/Driver/GetDriversToday', { Token: $scope.Token }).then(function (myResponse) {
            if (myResponse.IsSuccessful) {
                $scope.DriversToday = myResponse.response.data;
            }
            else
                console.log("An error occurred while getting Drivers.");
        });
    };

    $scope.GetNotifications = function () {
        SharedServices.Post('/Notification/GetNotificationsToday').then(function (myResponse) {
            if (myResponse.IsSuccessful)
                $scope.Notifications = myResponse.response.data;
            else
                console.log("An error occurred while getting Notifications.");
        });
    };

    $scope.GetTripPath = function (tripId) {
        SharedServices.Post('/TripBackend/GetTripPath', { TripId: tripId, Token: $scope.Token }).then(function (myResponse) {
            if (myResponse.IsSuccessful) {
                $scope.TripPath = myResponse.response.data;
                $scope.TripPath.Stops.push($scope.TripPath.Destination);
                
                $scope.initMap();
            }
            else
                console.log("An error occurred while getting Trip's path.");
        });
    };
}]);