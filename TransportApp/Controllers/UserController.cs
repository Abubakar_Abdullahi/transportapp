﻿// RECOVERY FIXED

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TransportApp.Models;
using TransportApp.Security;
using static TransportApp.Controllers.Tools;

namespace TransportApp.Controllers
{
    public class UserController : Controller
    {
        private TransportEntities db = new TransportEntities();

        public ActionResult Index()
        {
            var RequestToken = Request.Cookies.Get("Token");
            if (RequestToken != null && JwtManager.ValidateToken(RequestToken.Value, ManagerRole) != 0)
            {
                ViewBag.Name = Request.Cookies.Get("Name").Value;
                return View();
            }
            else
            {
                ViewBag.RedirectLocation = "/User";
                return RedirectToAction("Login");
            }
        }

        public ActionResult Login()
        {   
            HttpCookie RequestToken = Request.Cookies.Get("Token");
            if (RequestToken != null && JwtManager.ValidateToken(RequestToken.Value, ManagerRole) != 0)
            {
                ViewBag.Name = Request.Cookies.Get("Name").Value;
                return RedirectToAction("Index", "Monitor");
            }
            else
                return View();
        }

        public JsonResult GetRoles(string serverInput)
        {
            JsonResult result = new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            string token;

            try
            {
                token = JsonConvert.DeserializeObject<UserData>(serverInput).Token;
            }
            catch
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "The argument passed could not be processed.";
                result.Data = "The argument passed could not be processed.";
                return result;
            }

            if (JwtManager.ValidateToken(token, AllRoles) == 0)
            {
                Response.StatusCode = 401;
                result.Data = "User does not have access to the resource.";
                return result;
            }

            try
            {
                result.Data = db.Roles.Select(role => new
                {
                    role.RoleId,
                    role.RoleName
                }).ToList();
            }
            catch
            {
                Response.StatusCode = 500;
                Response.StatusDescription = "Roles list could not be retrieved.";
                result.Data = "Roles list could not be retrieved.";
            }

            return result;
        }

        public JsonResult SaveUser(string serverInput)
        {
            JsonResult result = new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            SaveUserData userData;

            try
            {
                userData = JsonConvert.DeserializeObject<SaveUserData>(serverInput);
            }
            catch
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "The argument passed could not be processed.";
                result.Data = "The argument passed could not be processed.";
                return result;
            }

            if (JwtManager.ValidateToken(userData.Token, ManagerRole) == 0)
            {
                Response.StatusCode = 401;
                result.Data = "User does not have access to the resource.";
                return result;
            }

            if (userData.UserId == 0)
            {
                try
                {
                    User newUser = new User
                    {
                        RoleId = userData.RoleId,
                        Name = userData.Name,
                        Surname = userData.Surname,
                        Gender = userData.Gender,
                        PhoneNo = userData.PhoneNo,
                        Email = userData.Email,
                        Password = "secret"
                    };
                    db.Users.Add(newUser);
                    db.SaveChanges();

                    newUser.Username = newUser.Name + newUser.Surname + newUser.RoleId + newUser.UserId;
                    string roleName = userData.RoleName;

                    switch (userData.RoleName)
                    {
                        case "Driver":
                            db.Drivers.Add(new Driver()
                            {
                                DriverId = newUser.UserId
                            });
                            break;
                        case "Passenger":
                            db.Passengers.Add(new Passenger
                            {
                                PassengerId = newUser.UserId,
                                BeaconId = userData.BeaconId ?? null
                            });
                            break;
                        default:
                            break;
                    }

                    db.SaveChanges();
                    result.Data = "User was added successfully.";
                }
                catch
                {
                    Response.StatusCode = 500;
                    Response.StatusDescription = "User could not be added.";
                    result.Data = "User could not be added.";
                }
            }
            else
            {
                try
                {
                    User user = db.Users.Find(userData.UserId);
                    if (user == null)
                    {
                        Response.StatusCode = 404;
                        Response.StatusDescription = "User with the provided UserId does not exist.";
                        result.Data = "User with the provided UserId does not exist.";
                        return result;
                    }

                    user.Name = userData.Name;
                    user.Surname = userData.Surname;
                    user.Username = userData.Name + userData.Surname + userData.RoleId + userData.UserId;
                    user.Gender = userData.Gender;
                    user.PhoneNo = userData.PhoneNo;
                    user.Email = userData.Email;

                    if (user.Role.RoleName == "Passenger")
                    {
                        int[] toBeRemovedTripIds = user.Passenger.TripPassengers.Select(tp => tp.TripId).Except(userData.Options).ToArray();
                        int[] newTripIds = userData.Options.Except(user.Passenger.TripPassengers.Select(tp => tp.TripId).ToArray()).ToArray();

                        foreach (int tripId in toBeRemovedTripIds)
                            user.Passenger.TripPassengers.Remove(user.Passenger.TripPassengers.Single(tp => tp.TripId == tripId));

                        foreach (int tripId in newTripIds)
                        {
                            user.Passenger.TripPassengers.Add(new TripPassenger
                            {
                                TripId = tripId,
                                SortingOrder = 0,
                                Coming = true
                            });
                        }
                        user.Passenger.BeaconId = userData.BeaconId ?? null;
                    }
                    else if (user.Role.RoleName == "Driver")
                    {
                        user.Driver.Trips.Clear();
                        user.Driver.Trips = userData.Options.Count() != 0 ? db.Trips.Where(trip => userData.Options.Contains(trip.TripId)).ToList() : null;
                    }

                    db.SaveChanges();
                    result.Data = "User has been edited successfully.";
                }
                catch
                {
                    Response.StatusCode = 500;
                    Response.StatusDescription = "User could not be edited.";
                    result.Data = "User could not be edited.";
                }
            }

            return result;
        }

        public JsonResult SaveDeviceId(string serverInput)
        {
            JsonResult result = new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            DeviceData deviceData;

            try
            {
                deviceData = JsonConvert.DeserializeObject<DeviceData>(serverInput);
                if (deviceData.UserId == 0 || deviceData.DeviceId == null || deviceData.DeviceId == "")
                    throw new Exception();
            }
            catch
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Arguement passed could not be processed.";
                result.Data = "Arguement passed could not be processed.";
                return result;
            }

            if (JwtManager.ValidateToken(deviceData.Token, AllRoles) == 0)
            {
                Response.StatusCode = 401;
                result.Data = "User does not have access to the resource.";
                return result;
            }

            User user = db.Users.Find(deviceData.UserId);
            if (user == null)
            {
                Response.StatusCode = 404;
                Response.StatusDescription = "A User with the provided UserId does not exist.";
                result.Data = "A User with the provided UserId does not exist.";
                return result;
            }
            user.DeviceId = deviceData.DeviceId;
            HttpWebRequest httpWebRequest = WebRequest.Create("https://onesignal.com/api/v1/players/" + deviceData.DeviceId) as HttpWebRequest;
            httpWebRequest.KeepAlive = true;
            httpWebRequest.Method = "PUT";
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            httpWebRequest.Headers.Add("authorization", OneSignalHeaderValue);
            Dictionary<string, string> role = new Dictionary<string, string>
            {
                { "Role", user.Role.RoleName }
            };

            JavaScriptSerializer scriptSerializer = new JavaScriptSerializer();
            var data = new
            {
                id = deviceData.DeviceId,
                app_id = OneSignalAppId,
                tags = role
            };
            byte[] bytes = Encoding.UTF8.GetBytes(scriptSerializer.Serialize(data));
            string message = null;
            try
            {
                using (Stream requestStream = httpWebRequest.GetRequestStream())
                    requestStream.Write(bytes, 0, bytes.Length);
                using (HttpWebResponse response = httpWebRequest.GetResponse() as HttpWebResponse)
                {
                    using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
                        message = streamReader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                Debug.WriteLine(ex.Message);
                Debug.WriteLine(new StreamReader(ex.Response.GetResponseStream()).ReadToEnd());
            }
            Debug.WriteLine(message);

            try
            {
                db.SaveChanges();
                result.Data = "DeviceId has been saved successfully.";
            }
            catch
            {
                Response.StatusCode = 500;
                Response.StatusDescription = "DeviceId could not be saved.";
                result.Data = "DeviceId could not be saved.";
            }
            return result;
        }

        [HttpPost]
        public JsonResult Login(string serverInput)
        {
            JsonResult result = new JsonResult();
            LoginCredentials userCredentials;

            try
            {
                userCredentials = JsonConvert.DeserializeObject<LoginCredentials>(serverInput);
            }
            catch
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "The argument passed could not be processed.";
                result.Data = "The argument passed could not be processed.";
                return result;
            }

            try
            {
                User user = db.Users.SingleOrDefault(usr => usr.Username == userCredentials.Username && usr.Password == userCredentials.Password);
                if (user == null)
                {
                    Response.StatusCode = 404;
                    Response.StatusDescription = "A User with the provided credentials does not exist.";
                    result.Data = "A User with the provided credentials does not exist";
                    return result;
                }
                result.Data = new
                {
                    user.UserId,
                    Name = (user.Name + " " + user.Surname),
                    user.RoleId,
                    user.Role.RoleName,
                    user.Email,
                    user.PhoneNo,
                    Token = JwtManager.GenerateToken(user)
                };
            }
            catch
            {
                Response.StatusCode = 401;
                Response.StatusDescription = "User could not be authenticated.";
                result.Data = "User could not be authenticated.";
                return result;
            }

            return result;
        }

        public JsonResult DeleteUser(string serverInput)
        {
            JsonResult result = new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            UserData userData;

            try
            {
                userData = JsonConvert.DeserializeObject<UserData>(serverInput);
            }
            catch
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "The argument passed could not be processed.";
                result.Data = "The argument passed could not be processed.";
                return result;
            }

            if (JwtManager.ValidateToken(userData.Token, ManagerRole) == 0)
            {
                Response.StatusCode = 401;
                result.Data = "User does not have access to the resource.";
                return result;
            }

            try
            {
                User user = db.Users.Find(userData.UserId);
                if (user == null)
                {
                    Response.StatusCode = 404;
                    Response.StatusDescription = "User with the provided UserId does not exist.";
                    result.Data = "User with the provided UserId does not exist.";
                    return result;
                }

                switch(user.Role.RoleName)
                {
                    case "Driver":
                        var driver = db.Drivers.Find(user.UserId);
                        driver.Trips.Clear();
                        db.Drivers.Remove(driver);
                        break;
                    case "Passenger":
                        var passenger = db.Passengers.Find(user.UserId);
                        passenger.BusCoordinates.Clear();
                        db.TripPassengers.RemoveRange(passenger.TripPassengers);
                        db.Passengers.Remove(passenger);
                        break;
                    default:
                        break;
                }

                db.Notifications.RemoveRange(user.Notifications);
                user.Notifications1.Clear();
                db.Users.Remove(user);
                db.SaveChanges();
                result.Data = "User has been deleted sucessfully";
            }
            catch
            {
                Response.StatusCode = 500;
                Response.StatusDescription = "User could not be deleted.";
                result.Data = "User could not be deleted.";
                return result;
            }

            return result;
        }
    }
}
