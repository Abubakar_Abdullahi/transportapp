﻿// Decompiled with JetBrains decompiler
// Type: TransportApp.Security.JwtManager
// Assembly: TransportApp, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9FC5505E-8A04-49A5-ADEF-D6AFF964F1CE
// Assembly location: C:\Users\Abubakar\Desktop\pfts\bin\TransportApp.dll

using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using TransportApp.Models;

namespace TransportApp.Security
{
    public static class JwtManager
    {
        private const string Secret = "db3OIsj+BXE9NZDy0t8W3TcNekrF+2d/1sFnWG4HnV8TZY30iTOdtVWJG8abWvB1GlOgJuQZdcF2Luqm/hccMw==";

        public static bool VerifySignature(JwtSecurityTokenHandler token)
        {
            Convert.FromBase64String("db3OIsj+BXE9NZDy0t8W3TcNekrF+2d/1sFnWG4HnV8TZY30iTOdtVWJG8abWvB1GlOgJuQZdcF2Luqm/hccMw==");
            return false;
        }

        public static string GenerateToken(User user, int expireMonths = 3)
        {
            byte[] key = Convert.FromBase64String("db3OIsj+BXE9NZDy0t8W3TcNekrF+2d/1sFnWG4HnV8TZY30iTOdtVWJG8abWvB1GlOgJuQZdcF2Luqm/hccMw==");
            JwtSecurityTokenHandler securityTokenHandler = new JwtSecurityTokenHandler();
            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[3]
                {
                    new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name", user.Username),
                    new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier", user.UserId.ToString()),
                    new Claim("http://schemas.microsoft.com/ws/2008/06/identity/claims/role", user.Role.RoleName)
                }),
                Expires = new DateTime?(DateTime.UtcNow.AddMonths(expireMonths)),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), "http://www.w3.org/2001/04/xmldsig-more#hmac-sha256"),
                Issuer = "TransportApp"
            };
            SecurityToken token = securityTokenHandler.CreateToken(tokenDescriptor);
            return securityTokenHandler.WriteToken(token);
        }

        public static int ValidateToken(string token, List<string> roles)
        {
            if (token == null)
                return 0;

            try
            {
                JwtSecurityToken jwtSecurityToken = new JwtSecurityTokenHandler().ReadToken(token) as JwtSecurityToken;

                int num = DateTime.Now.CompareTo(jwtSecurityToken.ValidTo);
                if (jwtSecurityToken.SignatureAlgorithm != "HS256" || jwtSecurityToken.Issuer != "TransportApp" || num >= 0 || !roles.Contains(jwtSecurityToken.Payload["role"].ToString()))
                    return 0;

                return Convert.ToInt32(jwtSecurityToken.Payload["nameid"]);
            }
            catch
            {
                return 0;
            }
        }

        public static ClaimsPrincipal GetPrincipal(string token)
        {
            try
            {
                JwtSecurityTokenHandler securityTokenHandler = new JwtSecurityTokenHandler();
                if (!(securityTokenHandler.ReadToken(token) is JwtSecurityToken))
                    return (ClaimsPrincipal)null;
                byte[] key = Convert.FromBase64String("db3OIsj+BXE9NZDy0t8W3TcNekrF+2d/1sFnWG4HnV8TZY30iTOdtVWJG8abWvB1GlOgJuQZdcF2Luqm/hccMw==");
                TokenValidationParameters validationParameters = new TokenValidationParameters()
                {
                    RequireExpirationTime = true,
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    IssuerSigningKey = (SecurityKey)new SymmetricSecurityKey(key)
                };
                return securityTokenHandler.ValidateToken(token, validationParameters, out SecurityToken validatedToken);
            }
            catch
            {
                return (ClaimsPrincipal)null;
            }
        }
    }
}
