﻿
TransportApp.controller('DeleteItemCtrl', ['$scope', '$uibModalInstance', 'passData', 'SharedServices', function ($scope, $uibModalInstance, passData, SharedServices) {
    
    $scope.deleteItem = function () {
        SharedServices.Post(passData.url, passData.data).then(function (myResponse) {
            if (myResponse.IsSuccessful) {
                $uibModalInstance.close();
                SharedServices.Alert(myResponse.response.data);
            }
            else {
                $uibModalInstance.dismiss();
                SharedServices.Alert("Error occurred while deleting item.");
            }   
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };

}]);